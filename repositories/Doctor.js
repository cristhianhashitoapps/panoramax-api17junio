var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Doctor, []);
var sequelize  = require('sequelize');
var moment     = require('moment');
var jwt        = require('jwt-simple');

repository.Validate = function(model) {
  var error = null;
  var opts  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

  if (model.telefono === undefined || model.telefono === null) {
    error = 'Debe de ingresar un teléfono válido';
  }
  if (model.correo === undefined || model.correo === null) {
    error = 'Debe de ingresar un correo válido';
  }
  if (model.nombres === undefined || model.nombres === null) {
    error = 'Debe de ingresar un nombre Válido';
  }

  var clave = [];

  for (var i = 0; i < 6; i++) {
    var rnd = Math.trunc(Math.random() * opts.length);

    clave.push(opts[rnd]);
  }

  model.clave = clave.join('');

  return error;
};


repository.LoadById = function(Id) {
  return this.entity
    .find({
      where: {Id: Id},
      include: this.includes
    });
};

repository.Login = function(loginData) {
  return models
    .Doctor
    .find({
      where: {
        telefono: loginData.telefono
      }
    });
};

repository.SignUp = function(signUpData) {

  console.log("signUpData:"+signUpData.Empresa);

  var insertarDireccion = true;

  return models
    .Doctor
    .find({
      where: {
        telefono: signUpData.telefono
      }
    })
    .then(function(result) {
      console.log(result);
      if (!result) {// la primera vez no existe

        console.log(signUpData);

        return repository.Insert(signUpData);
      } else {
        insertarDireccion = false;

        repository.Validate(result);

        return result.updateAttributes({
          clave: result.clave,

          correo: signUpData.correo
        });
      }
    })
    .then(function(user) {
      //console.log(user);
      var plivo = require('plivo');
      var p = plivo.RestAPI({
        authId: 'MANGZHZWQYZTG0YJM0YM',
        authToken: 'YzE0NzIwZGM3Njg4YzcwMmQ2MzBlOTMyMjZiZDlh'
      });

      var params = {
        src: '573104087556', // Sender's phone number with country code
        dst: ['57', user.telefono].join(''), // Receiver's phone Number with country code
        text: ['Hola, tu código de verificación es', user.clave].join(' '),  // Your SMS Text Message - English
        url: 'https://intense-brook-8241.herokuapp.com/report/', // The URL to which with the status of the message is sent
        method: 'GET' // The method used to call the url
      };

      p.send_message(params, function(status, response) {
        console.log('Status: ', status);
        console.log('API Response:\n', response);
      });

      if (insertarDireccion) {
        signUpData.Empresa.DoctorId = user.Id;

        models
          .Empresa
          .create(signUpData.Empresa);
      }

      return sequelize.Promise.resolve(user);
    });
};

repository.CreateToken = function(user) {
  var config = require('../config/config-jwt');

  var payload = {
    sub: user,
    iat: moment().unix(),
    exp: moment().add(10, 'years').unix(),
  };


  return jwt.encode(payload, config.TOKEN_SECRET);
};

repository.Update = function(Telefono, newUsuario) {
  return new Promise(function(resolve, reject) {
    console.log("update");
    models
      .Doctor
      .find({
        where: {
          Id: Telefono
        }
      })

      .then(function(model) {
        if (model === null) {
          return reject('Can\'t find object that Id');
        }

        return resolve(model.updateAttributes(newUsuario));
      });
  });
};

repository.GetEmpresas = function(Id) {
  return models
    .Empresa
    .findAll({
      where: {
        DoctorId: Id
      },
      include: [
      {
        model: models.Doctor
      }
    ]})
}

repository.GetOrdersPanoramax = function(PId){
  return models
    .SolicitudExamenes
    .findAll({
      limit: 40,
      distinct: 'SolicitudExamenes.Id',
      where: {
        DoctorId: PId
      },
      include: [
        {
          model: models.Doctor
        },
        {
          model: models.Paciente
        }, {
          model: models.TipoServicio
        },
        {
          model: models.SolicitudTipoEntrega,
          include: models.TipoEntregaResultados
        },
         /*{
          model: models.DetallesExamenes,
          include: [{
            model: models.SubCategoria
          }]
        }*/
      ],
      order: 'createdAt DESC'
    });
};

repository.GetDoctores = function(){


  return models
    .Doctor
    .findAll({
      where: {

      },
    })


}


module.exports = repository;
