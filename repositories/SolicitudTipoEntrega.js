var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.SolicitudTipoEntrega, []);

var includes   = [{
    model: models.TipoEntregaResultados,
  }];

repository.InsertarSolicitudTipoEntrega = function(data){
  var promises = [];
  [].forEach.call(data, function(tipo) {
    promises.push(models.SolicitudTipoEntrega.create(tipo));
  });

  return Promise.all(promises);
};

repository.LoadBySolicitudId = function(Id){
	return new Promise(function(resolve, reject) {
    models.SolicitudTipoEntrega
      .find({
        where: {SolicitudExameneId: Id},
        include: includes
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
};

module.exports = repository;
