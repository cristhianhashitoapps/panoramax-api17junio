var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.DetallesExamenes, []);

/*repository.Validate = function(model) {
  var error = null;

  if (model.campoAbierto === undefined || model.campoAbierto === null) {
    error = 'Debe de proporcionar una descripcion del campo abierto del examen';
  }

  return error;
};*/

repository.InsertarDetallePedido = function(data){
var promises = [];
    [].forEach.call(data, function(det) {
      promises.push(models.DetallesExamenes.create(det));
    });

    return Promise.all(promises);
};

repository.CargarDetallesPorSolicitudExamen = function(Id) {
  return this.entity
    .findAll({
      where: {SolicitudExameneId: Id},
      include: [
        {
          model: models.SubCategoria,
          include: [
            {
              model: models.Categoria
            }
          ]
        }
      ]
    });
};

module.exports = repository;
