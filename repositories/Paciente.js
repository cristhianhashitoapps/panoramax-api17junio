var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Paciente, []);

repository.Validate = function(model) {
  var error = null;

  if (model.nombres === undefined || model.nombres === null) {
    error = 'Debe de proporcionar un nombre del paciente valido';
  }

  if (model.apellidos === undefined || model.apellidos === null) {
    error = 'Debe de proporcionar un apellido del paciente valido';
  }

  if (model.telefono === undefined || model.telefono === null) {
    error = 'Debe de proporcionar un numero de telefono del paciente valido';
  }

  if (model.edad === undefined || model.edad === null) {
    error = 'Debe de proporcionar una edad del paciente valida';
  }

  if (model.email === undefined || model.email === null) {
    error = 'Debe de proporcionar un correo del paciente valido';
  }

  if (model.cedula === undefined || model.cedula === null) {
    error = 'Debe de proporcionar una cedula del paciente valida';
  }
  if (model.fechaNacimiento === undefined || model.fechaNacimiento === null) {
    error = 'Debe de proporcionar una feha de nacimiento del paciente valida';
  }

  return error;
};

repository.GetAllLikeName = function(name) {
  return this.entity
    .findAndCountAll({
      where: {
        nombres: {
          $like: '%' + name + '%'
        }
      }     
      
    });
};

module.exports = repository;
