var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.SolicitudExamenes, []);

repository.Validate = function(model) {
  var error = null;

  if (model.observaciones === undefined || model.observaciones== null) {
    error = 'Debe de proporcionar una observacion de solicitud valida';
  }

  return error;
};




module.exports = repository;
