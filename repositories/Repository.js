var models  = require('../models');
var Promise = require('bluebird');

module.exports = function(entity, include) {
  var repository = {
    entity: entity,
    includes: include
  };

  repository.Validate = function(model) {
    return null;
  };

  repository.LoadById = function(Id) {
    return this.entity
      .find({
        where: {Id: Id},
        include: this.includes
      });
  };

  repository.LoadPage = function(page, pageSize, orderBy, sortOrder) {
    return this.entity
      .findAndCountAll({
        offset: (page - 1) * pageSize,
        limit: pageSize,
        order: [orderBy, sortOrder].join(' '),
        include: this.includes
      });
  };

  repository.LoadByQuery = function(query) {
    return this.entity
      .findAll({
        where: query,
        include: this.includes
      });
  };

  repository.LoadPageByQuery =
  function(query, page, pageSize, orderBy, sortOrder) {
    return this.entity
      .findAndCountAll({
        where: query,
        offset: (page - 1) * pageSize,
        limit: pageSize,
        order: [orderBy, sortOrder].join(' '),
        include: this.includes
      });
  };

  repository.Insert = function(model) {
    return new Promise(function(resolve, reject) {
      var validations = repository.Validate(model);

      if (validations !== null) {
        return reject(validations);
      }

      return resolve(repository.entity.create(model));
    });
  };

  repository.Update = function(Id, newModel) {
    var Repository = this;
    return new Promise(function(resolve, reject) {
      Repository
        .LoadById(Id)
        .then(function(model) {
          if (model === null) {
            return reject('Can\'t find object that Id');
          }

          return resolve(model.updateAttributes(newModel));
        });
    });
  };

  repository.Delete = function(Id) {
    return this.entity
      .destroy({
        where: {Id: Id}
      });
  };

  return repository;
}
