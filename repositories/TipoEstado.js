var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.TipoEstado, []);

repository.Validate = function(model) {
  var error = null;

  if (model.estado === undefined || model.estado === null) {
    error = 'Debe de proporcionar un estado valido';
  }

  return error;
};

module.exports = repository;