var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var includes   = [{
    model: models.SubCategoria,
  }];
var repository = Repository(models.Categoria, includes);

repository.Validate = function(model) {
  var error = null;

  if (model.nombre === undefined || model.nombre === null) {
    error = 'Debe de proporcionar un nombre de categoria valido';
  }

  return error;
};

repository.LoadByTipoServicioId = function(Id) {
  return this.entity
    .findAndCountAll({
      where: {tiposervicios_id: Id},
      include: this.includes,
      order: [['orden','ASC']]
    });
};

module.exports = repository;
