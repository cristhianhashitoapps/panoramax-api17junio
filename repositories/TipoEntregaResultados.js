var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.TipoEntregaResultados, []);

repository.Validate = function(model) {
  var error = null;

  if (model.nombre === undefined || model.nombre === null) {
    error = 'Debe de proporcionar un nombre del paciente valido';
  }

  return error;
};

module.exports = repository;
