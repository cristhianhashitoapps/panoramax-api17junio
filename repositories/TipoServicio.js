var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.TipoServicio, []);

repository.Validate = function(model) {
  var error = null;

  if (model.nombre === undefined || model.nombre === null) {
    error = 'Debe de proporcionar un nombre de tipo de servicio valido';
  }

  if (model.descripcion === undefined || model.descripcion === null) {
    error = 'Debe de proporcionar una descripcion de tipo de servicio valido';
  }



  return error;
};

module.exports = repository;
