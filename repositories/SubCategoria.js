var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var includes   = [{
    model: models.Categoria
  }];
var repository = Repository(models.SubCategoria, includes);

repository.Validate = function(model) {
  var error = null;

  if (model.nombre === undefined || model.nombre== null) {
    error = 'Debe de proporcionar un nombre para una SubCategoria valida';
  }

  return error;
};

module.exports = repository;
