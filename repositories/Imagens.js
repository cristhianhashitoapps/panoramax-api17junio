var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Imagens, []);

repository.Validate = function(model) {
  var error = null;

  if (model.RutaGrande === undefined || model.RutaGrande === null) {
    error = 'Debe de ingresar una ruta grande válida';
  }
  if (model.RutaPequeño === undefined || model.RutaPequeño === null) {
    error = 'Debe de ingresar una ruta pequeña válida';
  }

  return error;
}

module.exports = repository;
