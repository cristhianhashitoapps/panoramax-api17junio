"use strict";

module.exports = function (sequelize,DataTypes) {

	var SolicitudExamenes = sequelize.define('SolicitudExamenes',{

		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		observaciones : { type: DataTypes.STRING, allowNull: true},
		createdAt: { type: DataTypes.DATE},
	}, {
		classMethods:{
			associate: function (models){
				/*SolicitudExamenes.hasOne(models.Paciente);*/
			/*	SolicitudExamenes.hasOne(models.Doctor);*/
				SolicitudExamenes.belongsTo(models.TipoServicio);
			/*	SolicitudExamenes.hasOne(models.TipoEstado);*/

				SolicitudExamenes.hasMany(models.SolicitudTipoEntrega);

				SolicitudExamenes.hasOne(models.DetallesExamenes);
				SolicitudExamenes.belongsTo(models.Paciente);
				SolicitudExamenes.belongsTo(models.Doctor);
				SolicitudExamenes.belongsTo(models.Empresa);


			}
		}

	});
	return SolicitudExamenes;
}
