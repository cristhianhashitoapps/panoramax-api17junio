"use strict";

module.exports = function (sequelize,DataTypes) {

	var Imagens = sequelize.define('Imagens',{

		id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		RutaGrande: { type: DataTypes.STRING, allowNull: false},
		RutaPequeño: { type: DataTypes.STRING, allowNull: false},
		createdAt: { type: DataTypes.DATE, allowNull: false},

	}, {
		classMethods:{
			associate: function (models){
				Imagens.belongsTo(models.Categoria);
			}
		}

	});
	return Imagens;
}
