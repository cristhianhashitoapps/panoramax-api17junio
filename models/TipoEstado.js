"use strict";

module.exports = function (sequelize,DataTypes) {

	var TipoEstado = sequelize.define('TipoEstado',{

		Id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
		estado: { type: DataTypes.STRING(45), allowNull: false},
		createdAt: { type: DataTypes.DATE, allowNull: false},

	}, {
		classMethods:{
			associate: function (models){
				TipoEstado.hasOne(models.SolicitudExamenes);
				}
			}
	});

	return TipoEstado;
}
