"use strict";

module.exports = function (sequelize,DataTypes) {

	var DetallesExamenes = sequelize.define('DetallesExamenes',{

		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		campoAbierto : { type: DataTypes.STRING(200), allowNull: true},
		createdAt: { type: DataTypes.DATE, allowNull: true},
	}, {
		classMethods:{
			associate: function (models){
				DetallesExamenes.belongsTo(models.SubCategoria);
				DetallesExamenes.belongsTo(models.SolicitudExamenes);

			}
		}

	});



	return DetallesExamenes;
}
