"use strict";

module.exports = function (sequelize,DataTypes) {

	var TipoEntregaResultados = sequelize.define('TipoEntregaResultados',{

		Id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
		nombre: { type: DataTypes.STRING(45), allowNull: false},
		createdAt: { type: DataTypes.DATE, allowNull: false},


	}, {
		classMethods:{
			associate: function (models){
				TipoEntregaResultados.hasOne(models.SolicitudTipoEntrega);
			}
		}
	});
	return TipoEntregaResultados;
}
