"use strict";

module.exports = function (sequelize,DataTypes) {

	var Empresa = sequelize.define('Empresa',{

		Id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
		nombre: {type: DataTypes.STRING(255), allowNull: false},
		tipoDireccion: {type: DataTypes.STRING(255), allowNull: false},
		Num1: {type: DataTypes.STRING(255), allowNull: false},
		Num2: {type: DataTypes.STRING(255), allowNull: false},
		Num3: {type: DataTypes.STRING(255), allowNull: false},
		Descripcion: {type: DataTypes.STRING(255), allowNull: false},
		createdAt: { type: DataTypes.DATE, allowNull: true},
	}, {
		classMethods:{
			associate: function (models){
				Empresa.belongsTo(models.Doctor);
				Empresa.hasMany(models.SolicitudExamenes);

			}
		}


	});
	return Empresa;
}
