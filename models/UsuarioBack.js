'use strict';

var Utils              = {};
var sequelize          = require('sequelize');
var jwt                = require('jwt-simple');
var moment             = require('moment');
var bcrypt             = sequelize.Promise.promisifyAll(require('bcryptjs'));
var SALT_WORK_FACTOR   = 10;
var MAX_LOGIN_ATTEMPTS = 5;
var LOCK_TIME          = 2 * 60 * 60 * 1000;

Utils.HashPassword = function(user, options) {
  if (user._changed && user._changed.CLAVE) {
    return bcrypt
      .genSaltAsync(SALT_WORK_FACTOR)
      .then(function(salt) {
        return bcrypt
          .hashAsync(user.CLAVE, salt)
      })
      .then(function(hash) {
        user.CLAVE = hash;

        return sequelize.Promise.resolve(user);
      });
  }
};

Utils.Login = function(EMAIL, CLAVE) {
  var models = require('../models');
  var User   = null;

  return models
    .UsuarioBack
    .find({
      where: {
        EMAIL: EMAIL
      }
    })
    .then(function(user) {
      if (!user) {
        return sequelize.Promise.reject('El usuario no existe.');
      }

      if (user.LOCKED) {
        return sequelize.Promise.reject('El usuario se encuentra bloqueado.');
      }

      User = user;

      return user.ComparePassword(CLAVE);
    })
    .then(function(isMatch) {
      if (!isMatch) {
        return sequelize.Promise.reject('Clave incorrecta.');
      }

      return User.updateAttributes({
        LAST_LOGIN: new Date(),
        LOCKED: false
      });
    })
    .then(function(user) {
      return sequelize.Promise.resolve(user);
    });
};

Utils.ComparePassword = function(password) {
  return bcrypt
    .compareAsync(password, this.CLAVE);
};

Utils.CreateToken = function(user) {
  var config = require('../config/config-jwt');
  var payload = {
    sub: user,
    iat: moment().unix(),
    exp: moment().add(1, 'days').unix(),
  };

  return jwt.encode(payload, config.TOKEN_BACK);
};

//-------------------------------------------------------------------------
// ENTIDAD  USUARIO
//-------------------------------------------------------------------------

module.exports = function(sequelize, DataTypes) {
  var UsuarioBack = sequelize.define('UsuarioBack', {
    EMAIL: {type: DataTypes.STRING(255), primaryKey: true},
    CLAVE: {type: DataTypes.STRING(255), allowNull: false},
    ENABLED: {type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true},
    INT_LOGIN: {type: DataTypes.INTEGER, allowNull: false, defaultValue: 0},
    LAST_LOGIN: {type: DataTypes.DATE, allowNull: true},
    LOCKED: {type: DataTypes.BOOLEAN, allowNull: true},
    EXPIRED: {type: DataTypes.BOOLEAN, allowNull: true},
    EXPIRES_AT: {type: DataTypes.DATE, allowNull: true},
    CONFIRMATION_TOKEN: {type: DataTypes.STRING(255), allowNull: true},
    PASSWORD_REQUESTED_AT: {type: DataTypes.DATE, allowNull: true},
    ROLES: {type: DataTypes.TEXT, allowNull: true},
    CREDENTIALS_EXPIRED: {type: DataTypes.BOOLEAN, allowNull: true},
    CREDENTIALS_EXPIRE_AT: {type: DataTypes.DATE, allowNull: true}
  }, {
    hooks: {
      beforeCreate: Utils.HashPassword,
      beforeUpdate: Utils.HashPassword
    },
    classMethods: {
      Login: Utils.Login
    },
    instanceMethods: {
      ComparePassword: Utils.ComparePassword
    }
  });

  return UsuarioBack;
};
