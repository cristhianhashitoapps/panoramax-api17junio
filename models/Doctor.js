"use strict";

module.exports = function (sequelize,DataTypes) {

	var Doctor = sequelize.define('Doctor',{

		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		nombres: { type: DataTypes.STRING(255), allowNull: false},
		apellidos: { type: DataTypes.STRING(255), allowNull: false},
		telefono: { type: DataTypes.INTEGER},
		clave:{ type:DataTypes.STRING(255)},
		correo: { type: DataTypes.STRING(255),allowNull: false},
		createdAt: { type: DataTypes.DATE , allowNull: false},
		UserPushId: {type: DataTypes.STRING, allowNull: true},
		DevicePushId: {type: DataTypes.STRING, allowNull: true}

	}, {
		classMethods:{
			associate: function (models){

		  /*  Doctor.belongsTo(models.SolicitudExamenes);*/
			 Doctor.hasOne(models.SolicitudExamenes);
			 Doctor.hasMany(models.Empresa);

			}
		}

	});
	return Doctor;
}
