"use strict";

module.exports = function (sequelize,DataTypes) {

	var Categoria = sequelize.define('Categoria',{

		Id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
		nombre: { type: DataTypes.STRING(45), allowNull: false},
		createdAt: { type: DataTypes.DATE, allowNull: false},
		Imagen: { type: DataTypes.STRING(200), allowNull: true},
		tiposervicios_id: {type: DataTypes.INTEGER, allowNull: false},
		clase: { type: DataTypes.STRING(200), allowNull: true},

	}, {
		classMethods:{
			associate: function (models){
				Categoria.hasMany(models.Imagens);
				Categoria.hasMany(models.SubCategoria);
			}
		}


	});


	return Categoria;

}
