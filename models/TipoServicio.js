"use strict";

module.exports = function (sequelize,DataTypes) {

	var TipoServicio = sequelize.define('TipoServicio',{

		Id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
		nombre: { type: DataTypes.STRING(45), allowNull: false},
		descripcion: { type: DataTypes.STRING(45), allowNull: false},
		createdAt: { type: DataTypes.DATE, allowNull: false},
		Imagen: { type: DataTypes.STRING(45), allowNull: true},
	}, {
		classMethods:{
			associate: function (models){
				TipoServicio.hasMany(models.SolicitudExamenes);

			}
		}

	});
	return TipoServicio;

}
