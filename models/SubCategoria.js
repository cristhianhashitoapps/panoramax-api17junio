"use strict";

module.exports = function (sequelize,DataTypes) {

	var SubCategoria = sequelize.define('SubCategoria',{

		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		nombre : { type: DataTypes.STRING(200), allowNull: false},
		clase : { type: DataTypes.STRING(100)},
		campo: { type:DataTypes.STRING(100)},
		createdAt: { type: DataTypes.DATE, allowNull: false},
		ImagenPrincipal: { type:DataTypes.STRING(200)},
		CssClass: { type:DataTypes.STRING(200)},

	}, {
		classMethods:{
			associate: function (models){
				SubCategoria.belongsTo(models.Categoria);
				SubCategoria.hasMany(models.DetallesExamenes);

			}
		}


	});

	return SubCategoria;

}
