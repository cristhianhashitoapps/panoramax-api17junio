"use strict";

module.exports = function (sequelize,DataTypes) {

	var SolicitudTipoEntrega = sequelize.define('SolicitudTipoEntrega',{

		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		createdAt: { type: DataTypes.DATE, allowNull: false},
	}, {
		classMethods:{
			associate: function (models){
				SolicitudTipoEntrega.belongsTo(models.TipoEntregaResultados);
			  SolicitudTipoEntrega.belongsTo(models.SolicitudExamenes);
			}
		}


	});
	return SolicitudTipoEntrega;
}
