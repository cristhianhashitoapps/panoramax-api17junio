"use strict";

module.exports = function (sequelize,DataTypes) {

	var Paciente = sequelize.define('Paciente',{

		Id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
		nombres: { type: DataTypes.STRING(255), allowNull: false},
		apellidos: { type: DataTypes.STRING(255), allowNull: false},
		telefono: { type: DataTypes.STRING(10)},
		edad: { type: DataTypes.INTEGER},
		email: { type: DataTypes.STRING(255), allowNull: false},
		cedula: { type: DataTypes.INTEGER, allowNull: false},
		fechaNacimiento: { type: DataTypes.DATE, allowNull: false},
		createdAt: { type: DataTypes.DATE, allowNull: false},

	}, {
		classMethods:{
			associate: function (models){
				Paciente.hasOne(models.SolicitudExamenes);

			}
		}
	});
	return Paciente;
}
