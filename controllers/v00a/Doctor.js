var models     = require('../../models');
var repository = require('../../repositories/Doctor');
var controller = require('./BaseController')(repository);

//para enviar mensajes
var admin = require("firebase-admin");

var serviceAccount = require("../../config/panoramax-notifications-firebase-adminsdk-46mc6-9aebc48de7.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  
});


controller.Login = function(req, res) {
  var loginData = req.body;

  console.log(loginData);

  repository
    .Login(loginData)
    .then(function(user) {
      if (!user) {
        res.status(404).send('La información de registro es incorrecta');
      } else if (user.clave != loginData.clave) {
        res.status(404).send('El código de activación es incorrecto');
      } else {
        /*user.updateAttributes({
          NumIntentos: 0
        })*/

        res.send({
          token: repository.CreateToken(user)
        });
      }
    })
    .catch(function(error) {
      console.log(error);
      res.status(500).send(error);
    });
};


controller.SignUp = function(req, res) {
  var signUpData = req.body;

  repository
    .SignUp(signUpData)
    .then(function(result) {
      console.log('*******************************************');
      console.log('*******************************************');
      console.log(result.dataValues);
      console.log('*******************************************');
      console.log('*******************************************');

      res.send(result);
    })
    .catch(function(error) {
      console.log(error);
      res.status(500).send(error);
    });
};

controller.GetOrdersPanoramax = function(req,res){
    var params = req.params;
    console.log(params.Id);
    //res.status(200).send("hola");
    repository
      .GetOrdersPanoramax(params.Id)
      .then(function(result){
        console.log('*******************************************');
        console.log('*******************************************');
        console.log(result);
        console.log('*******************************************');
        console.log('*******************************************');

        res.send(result);
      })
      .catch(function(error){
        console.log(error);
        res.status(500).send(error);
      });
};

controller.GetEmpresas = function(req, res) {
  var params = req.params;

  repository
    .GetEmpresas(params.Id)
    .then(function(result) {
      res.send(result);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
}

controller.NotificacionUno = function(req,res){
  var body = req.body; 

  var registrationToken = body.token;

  var payload = {
    notification:{
        "title": "Panoramax",
        "body": body.mensaje,
        "notId": "10"
    },

    data: {
      mensaje: body.mensaje     
    }
  };

  admin.messaging().sendToDevice(registrationToken, payload)
    .then(function(response) {
      console.log("Successfully sent message:", response);
    })
    .catch(function(error) {
      console.log("Error sending message:", error);
    });

    res.send(200);

  }


controller.NotificacionTodos = function(req,res){
  var body = req.body; 
  
  var payload = {
    notification:{
        "title": "Panoramax",
        "body": body.mensaje,
        "notId": "10"
    },
    data: {
      mensaje: body.mensaje      
    }
  };  

  var promises=[];

  repository
    .GetDoctores()
    .then(function(response){
      response.forEach(function(x){
        //promises.push(

        var p1 = new Promise(

            function(resolve,reject){
              admin.messaging().sendToDevice(x.DevicePushId, payload)
              .then(function(responsefull) {
                console.log("Successfully sent message:", responsefull);
              })
              .catch(function(error) {
                console.log("Error sending message:", error);
              })
            }
          );

        p1.then(console.log("Se hizo bien")).catch("Se hizo mal");



          //);
      })
      
      /*Promise.all(promises)
        .then(function(req,resultPromise){
          console.log("listo");
          res.status(200).send(resultPromise);
          
        })
        .catch(function(errPromise){
          console.log(errPromise);
          res.status(200).send(errPromise);
        })*/
    })
    .catch(function(err){
      res.status(500).send(err);
    })

  }  


module.exports = controller;
