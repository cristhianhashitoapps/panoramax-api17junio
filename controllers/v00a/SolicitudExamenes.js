var repository = require('../../repositories/SolicitudExamenes');
var controller = require('./BaseController')(repository);
var doctorRepository = require('../../repositories/Doctor');
var pacienteRepository = require('../../repositories/Paciente');
var empresaRepository = require('../../repositories/Empresa');
var solicitudTipoEntregaRepository = require('../../repositories/SolicitudTipoEntrega');
var moment     = require('moment');

var nodemailer    = require('nodemailer');


  var smtpTransport = require('nodemailer-smtp-transport');
  var transporter = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    auth: {
        user: 'panoramaxapp2017@gmail.com', // my mail
        pass: '2017apppanoramax'
    }
}));


controller.DarFecha = function(req,res){
  var fecha = moment("2017-07-01 15:35:33","YYYY-MM-DD hh:mm:ss").format("YYYY-MM-DD");
  var hora = moment("2017-07-01 15:35:33","YYYY-MM-DD hh:mm:ss").format("hh:mm");
  console.log(fecha);
  console.log(hora);
  res.status(200).send("OK");
}


controller.Insert = function(req,res,next){
  var data = req.body;
  console.log("data---"+data.pedido);
  console.log("data---"+data.detalles);
  repository
    .Insert(data.pedido)
    .then(function(result){
      console.log('*******************************************');
      console.log('*******************************************');
      console.log("insertando pedido");
      console.log('*******************************************');
      console.log('*******************************************');

      res.send(result);

      var promises=[];

      //necesario invocar
      req.body.pedido.Id = result.Id;

        console.log("id:-------->"+result.Id);

        var doctor = {};
        var paciente = {};
        var pedido = {};

        doctorRepository
          .LoadById(req.body.pedido.DoctorId)
          .then(function(resDoctor){

            doctor = resDoctor;

            pacienteRepository
              .LoadById(req.body.pedido.PacienteId)
              .then(function(resPaciente){

                  paciente = resPaciente;


                  repository.
                    LoadById(req.body.pedido.Id)
                    .then(function(resPedido){

                        pedido = resPedido;

                        var tiposEntrega=[];
                        req.body.tiposolicitudentrega.forEach(function(tipo){
                          tiposEntrega.push(tipo.Nombre);
                        })


                        //correo paciente
                        var mailOptions = {
                          from: 'Panoramax Ltda. <info@panoramaxltda.net>',
                          to: paciente.email,
                          subject: 'Confirmación orden: '+req.body.pedido.Id
                        };

                        //mailOptions.text = body.Message;
                        var espacio = "<br>";
                        var dobleespacio = "<br><br>";

                        var talo="202";//Estudios de ortodoncia
                        var entr="2do piso.";
                        var paqu="PAQUETE DE ESTUDIOS";
                        var emailsegundo = "talonarios202@panoramaxltda.net";

                        if(pedido.TipoServicioId==2){//Radiografías y tomografías
                          talo="101";
                          entr="1er piso.";
                          paqu="SERVICIOS";
                          emailsegundo="talonarios101@panoramaxltda.net";
                        }

                        var cabecera1 = ["TALONARIO ONLINE",talo,"- Panoramax Ltda"].join(" ");
                        var fecha = "Día: "+moment(pedido.createdAt,"YYYY-MM-DD hh:mm:ss").format("DD/MM/YYYY");
                        var hora = "Hora: "+moment(pedido.createdAt,"YYYY-MM-DD hh:mm:ss").format("hh:mm");
                        var numconfirmacion = 'Nro. Confirmación: '+pedido.Id;


                        var entrada = ["Entrada:",entr].join(" ");


                        var datosPaciente = "DATOS DEL PACIENTE";
                        var nombre = ["Nombre: ",paciente.nombres,paciente.apellidos].join(' ');
                        var edad = ["Edad:",paciente.edad].join(' ');
                        var telefono = ["Telefono:",paciente.telefono,' '];
                        var email = ["Email:",paciente.email].join(' ');
                        var documento = ["Documento:",paciente.cedula].join(' ');
                        var fechaNacimiento = ["Fecha Nacimiento:",moment(paciente.fechaNacimiento,"YYYY-MM-DD hh:mm:ss").format("MM/DD/YYYY")].join(' ');

                        var datosDoctor = "DATOS DEL DOCTOR"
                        var nombreDoctor = ["Doctor: ",doctor.nombres,doctor.apellidos].join(' ');
                        var telefonoDoctor = ["Telefono: ",doctor.nombres,doctor.telefono].join(' ');
                        var emailDoctor = ["Email: ",doctor.correo].join(' ');

                        var espaciolinea = "________________________________________";

                        var presentando = ['Presentando el número de confirmación (',pedido.Id,')','<br> en la caja le atenderemos con mayor facilidad.'].join(' ');
                        var presentando_sms = ['Presentando el número de confirmación (',pedido.Id,')','en la caja le atenderemos con mayor facilidad.'].join(' ');
                        var presentando2 = 'Recuerde que no se requiere de citas, <br>¡podrá ser atendido en cualquier momento!';
                         var presentando2_sms = 'Recuerde que no se requiere de citas, ¡podrá ser atendido en cualquier momento!';

                        var infoPanoramax= 'Panoramax Ltda. <br> Calle 49 No.9-21 <br> 338-4338,<br>338-4314';
                        var infoPanoramax_sms= 'Panoramax Ltda. Calle 49 No.9-21 338-4338, 338-4314';

                        var formasEntrega = 'FORMAS DE ENTREGA';
                        var tiposEntregaString = tiposEntrega.join('<br>');

                        var paqueteEstudios = paqu;

                        var observaciones = "OBSERVACIONES:";
                        var observacionesTexto = pedido.observaciones;



                        mailOptions.html = [cabecera1,dobleespacio,fecha,hora,espacio,numconfirmacion,entrada,
                        espacio,datosPaciente,nombre,edad,telefono,email,documento,fechaNacimiento,espacio,datosDoctor,
                        nombreDoctor,telefonoDoctor,emailDoctor,espacio,presentando,presentando2,espacio,infoPanoramax].join('<br>');



                        transporter.sendMail(mailOptions, function(error, info) {
                          if (error) {
                            console.log("Error enviando mensaje: "+error);
                          } else {
                            console.log('Message sent: ' + info.response);
                          }
                        });
                        //fin correo cliente

                        //correo doctor

                         //correo paciente
                        var mailOptionsDoc = {
                          from: 'Panoramax Ltda. <info@panoramaxltda.net>',
                          to: doctor.correo,
                          subject: 'Confirmación orden: '+req.body.pedido.Id
                        };

                        var mailOptionsDocSegundo = {
                          from: 'Panoramax Ltda. <info@panoramaxltda.net>',
                          to: emailsegundo,
                          subject: 'Confirmación orden: '+req.body.pedido.Id
                        };


                        //organizar categorias y subcategorias

                        var pedido = [];
                        var orden = req.body;

                        for(var i=0; i<orden.detalles.length; i++){

                          var categoria = {
                            CategoriaId : orden.detalles[i].CategoriaId,
                            CategoriaNombre : orden.detalles[i].CategoriaNombre,
                            SubCategorium : []
                          }

                          var subCategoria = {
                            campoAbierto: orden.detalles[i].campoAbierto,
                            SubCategoriumId: orden.detalles[i].SubCategoriumId,
                            SubCategoriumNombre: orden.detalles[i].SubCategoriumNombre,
                            SubCategoriumClase: orden.detalles[i].SubCategoriumClase
                          }

                          var categoriaExiste = false;
                          for(var j=0; j<pedido.length; j++){
                            if(pedido[j].CategoriaId == categoria.CategoriaId){
                              categoriaExiste = true;
                              pedido[j].SubCategorium.push(subCategoria);
                            }
                          }

                          if(!categoriaExiste){
                            categoria.SubCategorium.push(subCategoria);
                            pedido.push(categoria);
                          }
                        }
                        //imprime
                        var cadenaFinalCategorias = [];
                        for(var j=0; j<pedido.length; j++){
                          cadenaFinalCategorias.push(pedido[j].CategoriaNombre.toUpperCase() + ":<br/>");
                          //document.write(pedido[j].CategoriaNombre.toUpperCase() + ":<br/>");

                          for(var i=0; i<pedido[j].SubCategorium.length; i++){
                            if(pedido[j].SubCategorium[i].campoAbierto != "ninguno"){
                              cadenaFinalCategorias.push(pedido[j].SubCategorium[i].SubCategoriumNombre + ": "+pedido[j].SubCategorium[i].campoAbierto+"<br/>")
                              //document.write(pedido[j].SubCategorium[i].SubCategoriumNombre + ": "+pedido[j].SubCategorium[i].campoAbierto+"<br/>");
                            }
                            else{
                              if(pedido[j].SubCategorium[i].SubCategoriumClase!=undefined){
                                cadenaFinalCategorias.push(pedido[j].SubCategorium[i].SubCategoriumClase+"-"+pedido[j].SubCategorium[i].SubCategoriumNombre + "<br/>");

                              }else{
                                cadenaFinalCategorias.push(pedido[j].SubCategorium[i].SubCategoriumNombre + "<br/>");
                              }
                              //document.write(pedido[j].SubCategorium[i].SubCategoriumNombre + "<br/>");
                            }
                          }
                          cadenaFinalCategorias.push("<br/>");
                          //document.write("<br/>");
                        }
                        //fin organizar categorias


                        mailOptionsDoc.html = [cabecera1,dobleespacio,fecha,hora,espacio,numconfirmacion,entrada,
                        espacio,datosPaciente,nombre,edad,telefono,email,documento,fechaNacimiento,espacio,datosDoctor,
                        nombreDoctor,telefonoDoctor,emailDoctor,espacio,formasEntrega,tiposEntregaString,espacio,
                        paqueteEstudios,espacio,cadenaFinalCategorias.join(''),observaciones,observacionesTexto,espacio,
                        presentando,presentando2,espacio,infoPanoramax].join('<br>');

                        transporter.sendMail(mailOptionsDoc, function(error, info) {
                          if (error) {
                            console.log("Error enviando mensaje: "+error);
                          } else {
                            console.log('Message sent: ' + info.response);
                          }
                        });

                        //se envia email a panoramax
                        transporter.sendMail(mailOptionsDocSegundo, function(error, info) {
                          if (error) {
                            console.log("Error enviando mensaje: "+error);
                          } else {
                            console.log('Message sent: ' + info.response);
                          }
                        });

                        //fin correo doctor


                        //envio sms paciente
                        var plivo = require('plivo');
                        var p = plivo.RestAPI({
                          authId: 'MANGZHZWQYZTG0YJM0YM',
                          authToken: 'YzE0NzIwZGM3Njg4YzcwMmQ2MzBlOTMyMjZiZDlh'
                        });

                        var params = {
                          src: '573104087556', // Sender's phone number with country code
                          dst: ['57', paciente.telefono].join(''), // Receiver's phone Number with country code
                          text: [cabecera1,numconfirmacion,entrada,
                        presentando_sms,presentando2_sms,infoPanoramax_sms].join(' '),  // Your SMS Text Message - English
                          url: 'https://intense-brook-8241.herokuapp.com/report/', // The URL to which with the status of the message is sent
                          method: 'GET' // The method used to call the url
                        };

                        p.send_message(params, function(status, response) {
                          console.log('Status: ', status);
                          console.log('API Response:\n', response);
                        });
                        //fin envio sms paciente



                    })




              })


          })
          .catch(function(err){
            console.log(err);
          })







      next();
    })
    .catch(function(error){
      console.log(error);
      res.status(500).send(error);
    });
}

module.exports = controller;
