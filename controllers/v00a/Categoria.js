var repository = require('../../repositories/Categoria');
var controller = require('./BaseController')(repository);


controller.CargarPorTipoServicioId = function(req,res){
  var params = req.params;

  repository
    .LoadByTipoServicioId(params.Id)
    .then(function(result){
      res.send(result);
    })
    .catch(function(error){
      console.log(error);
      res.status(500).send(error);
    });


}



module.exports = controller;
