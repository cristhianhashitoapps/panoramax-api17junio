module.exports = function(Repository) {
  return {
    GetAll: function(req, res) {
      var page      = req.query.page      || 1;
      var pageSize  = req.query.pageSize  || 100;
      var orderBy   = req.query.orderBy   || 'createdAt';
      var sortOrder = req.query.sortOrder || 'ASC';

      Repository
        .LoadPage(page, pageSize, orderBy, sortOrder)
        .then(function(models) {
          res.send(models);
        })
        .catch(function(error) {
          res.status(500).send(error);
        });
    },
    GetById: function(req, res) {
      var params = req.params;

      Repository
        .LoadById(params.Id)
        .then(function(model) {
          if (model === null || model === undefined) {
            res.status(404).send('No existe un objeto con ese Id');
          } else {
            res.send(model);
          }
        })
        .catch(function(error) {
          res.status(500).send(error);
        });
    },
    Insert: function(req, res) {
      var body   = req.body;
      console.log("-------------------------------");
      console.log(body);
      console.log("-------------------------------");

      Repository
        .Insert(body)
        .then(function(model) {
          res.send(model);
        })
        .catch(function(error) {
          res.status(500).send(error);
        });
    },
    Update: function(req, res) {
      var params = req.params;
      var body   = req.body;

      Repository
        .Update(params.Id, body)
        .then(function(model) {
          if (model === null || model === undefined) {
            res.status(404).send('No existe un objeto con ese Id');
          } else {
            res.send(model);
          }
        })
        .catch(function(error) {
          res.status(500).send(error);
        });
    },
    Delete: function(req, res) {
      var params = req.params;

      Repository
        .Delete(params.Id)
        .then(function(model) {
          res.status(200).send('Ok');
        })
        .catch(function(error) {
          res.status(500).send(error);
        });
    }
  };
};
