var sequelize = require('sequelize');
var jwt       = require('jwt-simple');
var moment    = require('moment');
var Utils     = {};

Utils.CreateToken = function(user) {
  var config = require('../../config/config-jwt');

  console.log('CreateToken');

  var payload = {
    sub: user,
    iat: moment().unix(),
    exp: moment().add(14, 'days').unix(),
  };

  return jwt.encode(payload, config.TOKEN_BACK);
};

var models     = require('../../models');
var controller = require('./BaseController');

controller.GetAll = function(req, res) {
  models
    .UsuarioBack
    .findAll()
    .then(function(result) {
      res.send(result);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
};

controller.GetById = function(req, res) {
  var params = req.params;

  models
    .UsuarioBack
    .find({
      where: {
        EMAIL: params.Id
      }
    })
    .then(function(result) {
      res.send(result);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
};
controller.Insert = function(req, res) {
  var body = req.body;

  models
    .UsuarioBack
    .create(body)
    .then(function(result) {
      res.send(result);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
};
controller.Update = function(req, res) {
  var body   = req.body;
  var params = req.params;

  models
    .UsuarioBack
    .find({
      where: {
        EMAIL: params.Id
      }
    })
    .then(function(result) {
      if (result === null) {
        res.status(404).send('No existe un usuario con ese correo');
      }

      return result.updateAttributes(body);
    })
    .then(function(result) {
      res.send(result);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
};
controller.Delete = function(req, res) {
  var params = req.params;

  models
    .UsuarioBack
    .destroy({
      where: {
        EMAIL: params.Id
      }
    })
    .then(function(result) {
      res.send('OK');
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
};
controller.Login = function(req, res) {
  var body = req.body;

  models
    .UsuarioBack
    .Login(body.EMAIL, body.CLAVE)
    .then(function(user) {
      console.log(Utils.CreateToken(user));

      res.send({
        token: Utils.CreateToken(user)
      });
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
};

module.exports = controller;
