var repository = require('../../repositories/SolicitudTipoEntrega');
var controller = require('./BaseController')(repository);

controller.InsertarSolicitudTipoEntrega = function(req, res) {
  var data = req.body;

  repository
    .InsertarSolicitudTipoEntrega(data)
    .then(function(result) {
      res.send(result);
    })
    .catch(function(error) {
      res.status(500).send(error);
    })
}
module.exports = controller;
	