var repository = require('../../repositories/DetallesExamenes');
var controller = require('./BaseController')(repository);

controller.InsertarDetallePedido = function(req,res){
  var data = req.body;
  console.log("data---"+data);
  repository
    .InsertarDetallePedido(data)
    .then(function(result){
      console.log('*******************************************');
      console.log('*******************************************');
      console.log("insertando detalles");
      console.log('*******************************************');
      console.log('*******************************************');

      res.send(result);
    })
    .catch(function(error){
      console.log(error);
      res.status(500).send(error);
    });
}

controller.CargarDetallesPorSolicitudExamen = function(req,res){
  var data = req.params;

  repository
    .CargarDetallesPorSolicitudExamen(data.Id)
    .then(function(result){
      console.log('*******************************************');
      console.log('*******************************************');
      console.log("Consultando detalles");
      console.log('*******************************************');
      console.log('*******************************************');
      res.send(result);
    })
    .catch(function(error){
      console.log(error);
      res.status(500).send(error);
    });
}

module.exports = controller;
