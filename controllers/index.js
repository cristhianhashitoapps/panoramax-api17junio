var controllers = [];

controllers['v00'] = {
  User: require('./v00/user'),
};

controllers['v00a'] = {
  Categoria: require('./v00a/Categoria'),
  DetallesExamenes: require('./v00a/DetallesExamenes'),
  Doctor: require('./v00a/Doctor'),
  Empresa: require('./v00a/Empresa'),
  Imagens: require('./v00a/Imagens'),
  Paciente: require('./v00a/Paciente'),
  SolicitudExamenes: require('./v00a/SolicitudExamenes'),
  SolicitudTipoEntrega: require('./v00a/SolicitudTipoEntrega'),
  SubCategoria: require('./v00a/SubCategoria'),
  TipoEntregaResultados: require('./v00a/TipoEntregaResultados'),
  TipoEstado: require('./v00a/TipoEstado'),
  TipoServicio: require('./v00a/TipoServicio'),
  UsuarioBack: require('./v00a/UsuarioBack'),

};

module.exports = controllers;
