module.exports = {
	/**
	 * @api {get} /medioPago Consulta de todos los medios de pago
	 * @apiVersion 0.0.0
	 *
	 * @apiName ListarTodos
	 * @apiGroup MedioPagoController
	 * @apiDescription Este metodo retorna una lista de todos los medios de pago del sistema
	 *
	 *
	 * @apiSuccess {Number} Id del medio de pago
	 * @apiSuccess {String} Nombre del medio de pago
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Efectivo"
	 *		}]
	 * @apiError Medios de pago no encontrados
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existen medios de pago en el sistema actualmente"
 	 *     }
	 *
	 */
	ListarTodos: function(req, res) {
		res.send('respond with a resource');
	},	
	
	/**
	 * @api {put} /medioPago/:id Inserta un medio de pago en el sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName Insertar
	 * @apiGroup MedioPagoController
	 * @apiDescription Este método inserta un medio de pago en el sistema
	 *
	 * @apiParam {String} Nombre del medio de pago
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 201 Creado
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Efectivo"
	 *		}]
	 *
	 * @apiError Parametros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	Insertar: function(req, res) {
		res.send('respond with a resource');
	},

	/**
	 * @api {get} /medioPago/:id Consulta un medio de pago por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName CargarPorId
	 * @apiGroup MedioPagoController
	 * @apiDescription Este método retorna un medio de pago por su Id
	 *
	 * @apiParam {Number} Id del medio de pago
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Efectivo"
	 *		}]
	 * @apiError Medio de pago no encontrada
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existe medio de pago con el id {:id}"
 	 *     }
	 */
	CargarPorId: function(req, res) {
		res.send('respond with a resource');
	},

	/**
	 * @api {delete} /medioPago/:id Elimina un medio de pago por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName EliminarPorId
	 * @apiGroup MedioPagoController
	 * @apiDescription Este método elimina un medio de pago por su Id
	 *
	 * @apiParam {Number} Id del medio de pago
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		{
	 *			
	 *		}
	 *
	 * @apiError Medio de pago con relación
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 412 Falló precondición
 	 *     {
 	 *       "error": "El medio de pago con el id {:id} se usa en una relación, no es posible eliminar"
 	 *     }
	 */
	EliminarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

	/**
	 * @api {patch} /medioPago/:id Modifica un medio de pago por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName ModificarPorId
	 * @apiGroup MedioPagoController
	 * @apiDescription Este método modifica un medio de pago por su Id
	 *
	 * @apiParam {Number} Id del medio de pago
	 * @apiParam {String} Nombre del medio de pago
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Efectivo modificado"	
	 *		}]
	 *
 	 * @apiError Parámetros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	ModificarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

};