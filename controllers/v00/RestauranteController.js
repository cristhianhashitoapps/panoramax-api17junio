module.exports = {
	
	/**
	 * @api {get} /restaurante Consulta de todos los restaurantes
	 * @apiVersion 0.0.0
	 *
	 * @apiName ListarTodos
	 * @apiGroup RestauranteController
	 * @apiDescription Este metodo retorna una lista de todos los restaurantes del sistema
	 *
	 *
	 * @apiSuccess {Number} Id de la restaurante
	 * @apiSuccess {String} Nombre del restaurante
	 * @apiSuccess {String} Nit  del restaurante
	 * @apiSuccess {String} Dirección del restaurante
	 * @apiSuccess {String} Teléfono 1 restaurante
	 * @apiSuccess {String} Teléfono 2 restaurante
	 * @apiSuccess {String} Celular restaurante
	 * @apiSuccess {String} ValorPedidoMinimo restaurante
	 * @apiSuccess {String} ValorDomicilio restaurante
	 * @apiSuccess {String} PedidosOnLine si tiene
	 * @apiSuccess {String} PedidosTelefono si tiene
	 * @apiSuccess {String} EstadoRestaurante abierto / cerrado
	 * @apiSuccess {String} Latitud gps para localizar restaurante
	 * @apiSuccess {String} Longitud gps para localizar restaurante
	 * @apiSuccess {Imagen} Logo del restaurante
	 * @apiSuccess {CategoriaRestaurante} Categoria del restaurante
	 * @apiSuccess {MedioPago} Medios de pago permitidos por el restaurante
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Shushie Long"
	 *			"Nit": "123"
	 *			"Direccion": "Avenida los estudiantes"
	 *			"Telefono1": "999999"
	 *			"Telefono2": "888888"
	 *			"Celular": "3002979999"
	 *			"ValorPedidoMinimo": "30000"
	 *			"ValorDomicilio": "2500"
	 *			"PedidosOnLine": "true"
	 *			"PedidosTelefono": "true"
	 *			"EstadoRestaurante": "true"
	 *			"Latitud": "0.888"
	 *			"Longitud": "0.777"
	 *			"Imagen": 
	 *			{
	 *				"Id": "1",
	 *				"RutaGrande": "http://servidor/app/img.jpg",
	 *				"RutaPequeno": "http://servidor/app/img.jpg",
	 *			}
	 *			"CategoriaRestaurante": 
	 *			{
	 *				"Id": "1",
	 *				"Nombre": "Pollo asado"
	 *			}
	 *			"MedioPago": 
	 *			{
	 *				"Id": "1",
	 *				"Nombre": "Efectivo"
	 *			}
	 *		}]
	 * @apiError Restaurantes no encontrados
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existen restaurantes en el sistema actualmente"
 	 *     }
	 *
	 */
	ListarTodos: function(req, res) {
		res.send('respond with a resource');
	},	
	 
	/**
	 * @api {put} /restaurante/:id Inserta un restaurante en el sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName Insertar
	 * @apiGroup RestauranteController
	 * @apiDescription Este metodo inserta un restaurante en el sistema
	 *
	 *
	 * @apiParam {Number} Id de la restaurante
	 * @apiParam {String} Nombre del restaurante
	 * @apiParam {String} Nit  del restaurante
	 * @apiParam {String} Dirección del restaurante
	 * @apiParam {String} Teléfono 1 restaurante
	 * @apiParam {String} Teléfono 2 restaurante
	 * @apiParam {String} Celular restaurante
	 * @apiParam {String} ValorPedidoMinimo restaurante
	 * @apiParam {String} ValorDomicilio restaurante
	 * @apiParam {String} PedidosOnLine si tiene
	 * @apiParam {String} PedidosTelefono si tiene
	 * @apiParam {String} EstadoRestaurante abierto / cerrado
	 * @apiParam {String} Latitud gps para localizar restaurante
	 * @apiParam {String} Longitud gps para localizar restaurante
	 * @apiParam {Imagen} Logo del restaurante
	 * @apiParam {CategoriaRestaurante} Categoria del restaurante
	 * @apiParam {MedioPago} Medios de pago permitidos por el restaurante
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 201 Creado
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Shushie Long"
	 *			"Nit": "123"
	 *			"Direccion": "Avenida los estudiantes"
	 *			"Telefono1": "999999"
	 *			"Telefono2": "888888"
	 *			"Celular": "3002979999"
	 *			"ValorPedidoMinimo": "30000"
	 *			"ValorDomicilio": "2500"
	 *			"PedidosOnLine": "true"
	 *			"PedidosTelefono": "true"
	 *			"EstadoRestaurante": "true"
	 *			"Latitud": "0.888"
	 *			"Longitud": "0.777"
	 *			"Imagen": 
	 *			{
	 *				"Id": "1",
	 *				"RutaGrande": "http://servidor/app/img.jpg",
	 *				"RutaPequeno": "http://servidor/app/img.jpg",
	 *			}
	 *			"CategoriaRestaurante": 
	 *			{
	 *				"Id": "1",
	 *				"Nombre": "Pollo asado"
	 *			}
	 *			"MedioPago": 
	 *			{
	 *				"Id": "1",
	 *				"Nombre": "Efectivo"
	 *			}
	 *		}]
	 * @apiError Parámetros incorrectos
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud Incorrecta
 	 *     {
 	 *      	"error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 *
	 */ 
	Insertar: function(req, res) {
		res.send('respond with a resource');
	},

	/**
	 * @api {get} /restaurante/:id Consulta de restaurante por Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName CargarPorId
	 * @apiGroup RestauranteController
	 * @apiDescription Este metodo retorna  un restaurante por Id
	 *
	 *
	 * @apiSuccess {Number} Id de la restaurante
	 * @apiSuccess {String} Nombre del restaurante
	 * @apiSuccess {String} Nit  del restaurante
	 * @apiSuccess {String} Dirección del restaurante
	 * @apiSuccess {String} Teléfono 1 restaurante
	 * @apiSuccess {String} Teléfono 2 restaurante
	 * @apiSuccess {String} Celular restaurante
	 * @apiSuccess {String} ValorPedidoMinimo restaurante
	 * @apiSuccess {String} ValorDomicilio restaurante
	 * @apiSuccess {String} PedidosOnLine si tiene
	 * @apiSuccess {String} PedidosTelefono si tiene
	 * @apiSuccess {String} EstadoRestaurante abierto / cerrado
	 * @apiSuccess {String} Latitud gps para localizar restaurante
	 * @apiSuccess {String} Longitud gps para localizar restaurante
	 * @apiSuccess {Imagen} Logo del restaurante
	 * @apiSuccess {CategoriaRestaurante} Categoria del restaurante
	 * @apiSuccess {MedioPago} Medios de pago permitidos por el restaurante
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Shushie Long"
	 *			"Nit": "123"
	 *			"Direccion": "Avenida los estudiantes"
	 *			"Telefono1": "999999"
	 *			"Telefono2": "888888"
	 *			"Celular": "3002979999"
	 *			"ValorPedidoMinimo": "30000"
	 *			"ValorDomicilio": "2500"
	 *			"PedidosOnLine": "true"
	 *			"PedidosTelefono": "true"
	 *			"EstadoRestaurante": "true"
	 *			"Latitud": "0.888"
	 *			"Longitud": "0.777"
	 *			"Imagen": 
	 *			{
	 *				"Id": "1",
	 *				"RutaGrande": "http://servidor/app/img.jpg",
	 *				"RutaPequeno": "http://servidor/app/img.jpg",
	 *			}
	 *			"CategoriaRestaurante": 
	 *			{
	 *				"Id": "1",
	 *				"Nombre": "Pollo asado"
	 *			}
	 *			"MedioPago": 
	 *			{
	 *				"Id": "1",
	 *				"Nombre": "Efectivo"
	 *			}
	 *		}]
	 * @apiError Restaurantes no encontrado
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existe restaurante con el id {:id}"
 	 *     }
	 *
	 */
	CargarPorId: function(req, res) {
		res.send('respond with a resource');
	},

	/**
	 * @api {delete} /restaurante/:id Elimina un restaurante por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName EliminarPorId
	 * @apiGroup RestauranteController
	 * @apiDescription Este método elimina un restaurante por su Id
	 *
	 * @apiParam {Number} Id de restaurante
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		{
	 *			
	 *		}
	 *
	 * @apiError Restaurante con relacíon
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 412 Falló precondición
 	 *     {
 	 *       "error": "El restaurante con el id {:id} se usa en una relación, no es posible eliminar"
 	 *     }
	 */
	EliminarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

	/**
	 * @api {patch} /restaurante/:id Modifica un restaurante en el sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName ModificarPorId
	 * @apiGroup RestauranteController
	 * @apiDescription Este metodo modifica un restaurante en el sistema
	 *
	 *
	 * @apiParam {Number} Id de la restaurante
	 * @apiParam {String} Nombre del restaurante
	 * @apiParam {String} Nit  del restaurante
	 * @apiParam {String} Dirección del restaurante
	 * @apiParam {String} Teléfono 1 restaurante
	 * @apiParam {String} Teléfono 2 restaurante
	 * @apiParam {String} Celular restaurante
	 * @apiParam {String} ValorPedidoMinimo restaurante
	 * @apiParam {String} ValorDomicilio restaurante
	 * @apiParam {String} PedidosOnLine si tiene
	 * @apiParam {String} PedidosTelefono si tiene
	 * @apiParam {String} EstadoRestaurante abierto / cerrado
	 * @apiParam {String} Latitud gps para localizar restaurante
	 * @apiParam {String} Longitud gps para localizar restaurante
	 * @apiParam {Imagen} Logo del restaurante
	 * @apiParam {CategoriaRestaurante} Categoria del restaurante
	 * @apiParam {MedioPago} Medios de pago permitidos por el restaurante
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 Ok
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Shushie Long"
	 *			"Nit": "123"
	 *			"Direccion": "Avenida los estudiantes"
	 *			"Telefono1": "999999"
	 *			"Telefono2": "888888"
	 *			"Celular": "3002979999"
	 *			"ValorPedidoMinimo": "30000"
	 *			"ValorDomicilio": "2500"
	 *			"PedidosOnLine": "true"
	 *			"PedidosTelefono": "true"
	 *			"EstadoRestaurante": "true"
	 *			"Latitud": "0.888"
	 *			"Longitud": "0.777"
	 *			"Imagen": 
	 *			{
	 *				"Id": "1",
	 *				"RutaGrande": "http://servidor/app/img.jpg",
	 *				"RutaPequeno": "http://servidor/app/img.jpg",
	 *			}
	 *			"CategoriaRestaurante": 
	 *			{
	 *				"Id": "1",
	 *				"Nombre": "Pollo asado"
	 *			}
	 *			"MedioPago": 
	 *			{
	 *				"Id": "1",
	 *				"Nombre": "Efectivo"
	 *			}
	 *		}]
	 * @apiError Parámetros incorrectos
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud Incorrecta
 	 *     {
 	 *      	"error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 *
	 */ 
	ModificarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

};