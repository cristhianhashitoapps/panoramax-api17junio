module.exports = {
	/**
	 * @api {get} /categoriaRestaurante Consulta de todas las categorias del sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName ListarTodos
	 * @apiGroup CategoriaRestauranteController
	 * @apiDescription Este metodo retorna una lista de todas las categorias del sistema
	 *
	 *
	 * @apiSuccess {Number} Id de la categoria
	 * @apiSuccess {String} Nombre de la categoria
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Pollo asado"
	 *			
	 *		}]
	 * @apiError Categorias no encontradas
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existen categorías en el sistema actualmente"
 	 *     }
	 *

	 */
	ListarTodos: function(req, res) {
		res.send('respond with a resource');
	},	
	

	/**
	 * @api {put} /categoriaRestaurante/:id Inserta una categoria en el sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName Insertar
	 * @apiGroup CategoriaRestauranteController
	 * @apiDescription Este método inserta una categoria en el sistema
	 *
	 * @apiParam {String} Nombre de la categoria
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 201 Creado
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Pollo asado"
	 *		}]
	 *
	 * @apiError Parametros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	Insertar: function(req, res) {
		res.send('respond with a resource');
	},

	/**
	 * @api {get} /categoriaRestaurante/:id Consulta una categoría por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName CargarPorId
	 * @apiGroup CategoriaRestauranteController
	 * @apiDescription Este método retorna una categoría por su Id
	 *
	 * @apiParam {Number} Id de la categoría
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Pollo asado"
	 *		}]
	 * @apiError Categoria no encontrada
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existen categoría con el id {:id}"
 	 *     }
	 */
	CargarPorId: function(req, res) {
		res.send('respond with a resource');
	},

	
	/**
	 * @api {delete} /categoriaRestaurante/:id Elimina una categoria por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName EliminarPorId
	 * @apiGroup CategoriaRestauranteController
	 * @apiDescription Este método elimina una categoría por su Id
	 *
	 * @apiParam {Number} Id de la categoría
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		{
	 *			
	 *		}
	 *
	 * @apiError Categoría con relación
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 412 Falló precondición
 	 *     {
 	 *       "error": "La categoría con el id {:id} se usa en una relación, no es posible eliminar"
 	 *     }
	 */
	EliminarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

	/**
	 * @api {patch} /categoriaRestaurante/:id Modifica una categoría por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName ModificarPorId
	 * @apiGroup CategoriaRestauranteController
	 * @apiDescription Este método modifica una categoría por su Id
	 *
	 * @apiParam {Number} Id de la categoría
	 * @apiParam {String} Nombre de la categoría
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Pollo asado"
	 *		}]
	 *
 	 * @apiError Parámetros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	ModificarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

};