module.exports = {
	/**
	 * @api {get} /producto Consulta de todas los productos del sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName ListarTodos
	 * @apiGroup ProductoController
	 * @apiDescription Este metodo retorna una lista de productos
	 *
	 *
	 * @apiSuccess {Number} Id de la categoria
	 * @apiSuccess {String} Nombre del producto
	 * @apiSuccess {String} DescripcionCorta  del Producto
	 * @apiSuccess {String} DescripcionLarga  del Producto
	 * @apiSuccess {String} Precio  del Producto
	 * @apiSuccess {String} PorcentajePromo  del Producto
	 * @apiSuccess {Imagen} Imagen  del Producto
	 * @apiSuccess {CategoriaMenu} Categoría en la que se encuentra el Producto
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id":"1",
	 *			"Nombre":"Hamburguesa con pollo",
	 *			"DescrpcionCorta":"Hamburguesa de pollo papas y gaseosa",
	 *			"DescripcionLarga":"Deliciosa hamburguesa de pollo papas y gaseosa",
	 *			"Precio":"25000",
	 *			"PorcentajePromo":"30",
	 *			"TipoProducto":"P",	
	 *			"Imagen": {
	 *				"Id": "1",
	 *				"RutaGrande": "http://servidor/app/img.jpg",
	 *				"RutaPequeno": "http://servidor/app/img.jpg"
	 *			}
	 *			"CategoriaMenu": {
	 *				"Id": "1",
	 *				"Nombre": "Pollo asado"
	 *				"NombreCorto": "P.asado"
	 *			}
	 *		}]
	 * @apiError Categorias no encontradas
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existen categorías en el sistema actualmente"
 	 *     }
	 *
	 *	
	 */
	ListarTodos: function(req, res) {
		res.send('respond with a resource');
	},	
	
	/**
	 * @api {put} /producto/:id Inserta un producto en el sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName Insertar
	 * @apiGroup ProductoController
	 * @apiDescription Este metodo inserta un producto en el sistema
	 *
	 *
	 * @apiSuccess {Number} Id de la categoria
	 * @apiSuccess {String} Nombre del producto
	 * @apiSuccess {String} DescripcionCorta  del Producto
	 * @apiSuccess {String} DescripcionLarga  del Producto
	 * @apiSuccess {String} Precio  del Producto
	 * @apiSuccess {String} PorcentajePromo  del Producto
	 * @apiSuccess {Imagen} Imagen  del Producto
	 * @apiSuccess {CategoriaMenu} Categoría en la que se encuentra el Producto
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 201 Creado
	 *		[{
	 *			"Id":"1",
	 *			"Nombre":"Hamburguesa con pollo",
	 *			"DescrpcionCorta":"Hamburguesa de pollo papas y gaseosa",
	 *			"DescripcionLarga":"Deliciosa hamburguesa de pollo papas y gaseosa",
	 *			"Precio":"25000",
	 *			"PorcentajePromo":"30",
	 *			"TipoProducto":"P",	
	 *			"Imagen": {
	 *				"Id": "1",
	 *				"RutaGrande": "http://servidor/app/img.jpg",
	 *				"RutaPequeno": "http://servidor/app/img.jpg"
	 *			}
	 *			"CategoriaMenu": {
	 *				"Id": "1",
	 *				"Nombre": "Pollo asado"
	 *				"NombreCorto": "P.asado"
	 *			}
	 *		}]
	 *
 	 * @apiError Parametros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	Insertar: function(req, res) {
		res.send('respond with a resource');
	},

	/**
	 * @api {get} /producto/:id Consulta un producto por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName CargarPorId
	 * @apiGroup ProductoController
	 * @apiDescription Este metodo consulta un producto por su Id
	 *
	 *
	 * @apiParam {Number} Id del producto
	 *
	 * @apiSuccess {Number} Id de la categoria
	 * @apiSuccess {String} Nombre del producto
	 * @apiSuccess {String} DescripcionCorta  del Producto
	 * @apiSuccess {String} DescripcionLarga  del Producto
	 * @apiSuccess {String} Precio  del Producto
	 * @apiSuccess {String} PorcentajePromo  del Producto
	 * @apiSuccess {Imagen} Imagen  del Producto
	 * @apiSuccess {CategoriaMenu} Categoría en la que se encuentra el Producto
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id":"1",
	 *			"Nombre":"Hamburguesa con pollo",
	 *			"DescrpcionCorta":"Hamburguesa de pollo papas y gaseosa",
	 *			"DescripcionLarga":"Deliciosa hamburguesa de pollo papas y gaseosa",
	 *			"Precio":"25000",
	 *			"PorcentajePromo":"30",
	 *			"TipoProducto":"P",	
	 *			"Imagen": {
	 *				"Id": "1",
	 *				"RutaGrande": "http://servidor/app/img.jpg",
	 *				"RutaPequeno": "http://servidor/app/img.jpg"
	 *			}
	 *			"CategoriaMenu": {
	 *				"Id": "1",
	 *				"Nombre": "Pollo asado"
	 *				"NombreCorto": "P.asado"
	 *			}
	 *		}]
	 *
 	 * @apiError Producto no encontrado
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existe producto con el id {:id}"
 	 *     }
	 */
	CargarPorId: function(req, res) {
		res.send('respond with a resource');
	},

	/**
	 * @api {delete} /producto/:id Elimina un producto por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName EliminarPorId
	 * @apiGroup PedidoController
	 * @apiDescription Este método elimina producto por su Id
	 *
	 * @apiParam {Number} Id del producto
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		{
	 *			
	 *		}
	 *
	 * @apiError Producto con relación
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 412 Falló precondición
 	 *     {
 	 *       "error": "El producto con el id {:id} se usa en una relación, no es posible eliminar"
 	 *     }
	 */
	EliminarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

	/**
	 * @api {patch} /producto/:id Modifica un producto en el sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName ModificarPorId
	 * @apiGroup ProductoController
	 * @apiDescription Este metodo modifica un producto en el sistema
	 *
	 *
	 * @apiSuccess {Number} Id de la categoria
	 * @apiSuccess {String} Nombre del producto
	 * @apiSuccess {String} DescripcionCorta  del Producto
	 * @apiSuccess {String} DescripcionLarga  del Producto
	 * @apiSuccess {String} Precio  del Producto
	 * @apiSuccess {String} PorcentajePromo  del Producto
	 * @apiSuccess {Imagen} Imagen  del Producto
	 * @apiSuccess {CategoriaMenu} Categoría en la que se encuentra el Producto
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id":"1",
	 *			"Nombre":"Hamburguesa con pollo",
	 *			"DescrpcionCorta":"Hamburguesa de pollo papas y gaseosa",
	 *			"DescripcionLarga":"Deliciosa hamburguesa de pollo papas y gaseosa",
	 *			"Precio":"25000",
	 *			"PorcentajePromo":"30",
	 *			"TipoProducto":"P",	
	 *			"Imagen": {
	 *				"Id": "1",
	 *				"RutaGrande": "http://servidor/app/img.jpg",
	 *				"RutaPequeno": "http://servidor/app/img.jpg"
	 *			}
	 *			"CategoriaMenu": {
	 *				"Id": "1",
	 *				"Nombre": "Pollo asado"
	 *				"NombreCorto": "P.asado"
	 *			}
	 *		}]
	 *
 	 * @apiError Parametros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	ModificarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

};