module.exports = {
	
	/**
	 * @api {get} /pedido Consulta de todos los pedidos del sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName ListarTodos
	 * @apiGroup PedidoController
	 * @apiDescription Este metodo retorna una lista de todos los pedidos del sistema
	 *
	 *
	 * @apiSuccess {Number} Id del pedido
	 * @apiSuccess {Date} Fecha  Fecha y hora cuando se hizo el pedido
	 * @apiSuccess {Enum} Estado 
	 * @apiSuccess {Number} CalificacionUsuario 
	 * @apiSuccess {String} ComentarioUsuario 
	 * @apiSuccess {String} ObservacionUsuario 
	 * @apiSuccess {String} ObservacionEmpresa 
	 * @apiSuccess {MedioPago} MedioPago  
	 * @apiSuccess {Usuario} Usuario
	 * @apiSuccess {Restaurante} Restaurante
	 * @apiSuccess {DetallePedido} DetallePedido      
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"Fecha": "2014-01-01 05:25 pm",
	 *			"Estado": "A",
	 *			"CalificacionUsuario": "5",
	 *			"ComentarioUsuario": "Estuvo bien.",
	 *			"ObservacionUsuario": "Poner mucha salsa de tomate",
	 *			"ObservacionEmpresa": "Atender bien al cliente",
	 *			"MedioPago":
	 *			{
	 *				"Id": "1",
	 *				"Nombre" "Efectivo",
	 *			},
	 *			"Usuario":{
	 *				"Id": "1",
	 *				"Correo": "criferlo@gmail.com",
	 *				"Nombre" : "Cristhian Lombana",
	 *				"Direccion": "Cl 29 #11-12",
	 *				"Telefono": "7212121",
	 *				"Clave": "sdlfkd$$$%",
	 *			},	
	 *			"Restaurante":{
	 *				"Id": "1",
	 *				"Nombre": "SushieLong",
	 *				"Nit" : "89448484",
	 *				"Direccion": "Avenida los estudiantes",
	 *				"Telefono1": "7212121",
	 *				"Telefono2": "7210099",	
	 *				"Celular": "300999888"	,
	 *				"ValorPedidoMinimo": "50000",	
	 *				"ValorDomicilio": "2500",
	 *				"PedidosOnLine": "true"	,
	 *				"PedidosTelefono": "true"	,
	 *				"EstadoRestaurante": "true"	,
	 *				"Latitud": "0.10"	,
	 *				"Longitud": "0.10"	,					
	 *			},
	 *			"DetallePedido":
	 *			[
	 *				{
	 *					"id":"1",
	 *					"Cantidad":"3",
	 *					"PorcentajePromo":"20",
	 *					"Precio":"30.000",
	 *					"Producto":
	 *					{
	 *						"Id":"1",
	 *						"Nombre":"Hamburguesa con pollo",
	 *						"DescrpcionCorta":"Hamburguesa de pollo papas y gaseosa",
	 *						"DescripcionLarga":"Deliciosa hamburguesa de pollo papas y gaseosa",
	 *						"Precio":"25000",
	 *						"PorcentajePromo":"30",
	 *						"TipoProducto":"P",
	 *					}
	 *				},
	 *				
	 *			]	
	 *		}]
	 * @apiError Pedidos no encontrados
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existen pedidos en el sistema actualmente"
 	 *     }
	 *

	 */
	ListarTodos: function(req, res) {
		res.send('respond with a resource');
	},	

	/**
	 * @api {put} /pedido/:id Inserta un pedido en el sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName Insertar
	 * @apiGroup PedidoController
	 * @apiDescription Este método inserta un pedido en el sistema
	 *
	 * @apiSuccess {Number} Id del pedido
	 * @apiSuccess {Date} Fecha  Fecha y hora cuando se hizo el pedido
	 * @apiSuccess {Enum} Estado 
	 * @apiSuccess {Number} CalificacionUsuario 
	 * @apiSuccess {String} ComentarioUsuario 
	 * @apiSuccess {String} ObservacionUsuario 
	 * @apiSuccess {String} ObservacionEmpresa 
	 * @apiSuccess {MedioPago} MedioPago  
	 * @apiSuccess {Usuario} Usuario
	 * @apiSuccess {Restaurante} Restaurante
	 * @apiSuccess {DetallePedido} DetallePedido  
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 201 Creado
	 *		[{
	*			"Id": "1",
	 *			"Fecha": "2014-01-01 05:25 pm",
	 *			"Estado": "A",
	 *			"CalificacionUsuario": "5",
	 *			"ComentarioUsuario": "Estuvo bien.",
	 *			"ObservacionUsuario": "Poner mucha salsa de tomate",
	 *			"ObservacionEmpresa": "Atender bien al cliente",
	 *			"MedioPago":
	 *			{
	 *				"Id": "1",
	 *				"Nombre" "Efectivo",
	 *			},
	 *			"Usuario":{
	 *				"Id": "1",
	 *				"Correo": "criferlo@gmail.com",
	 *				"Nombre" : "Cristhian Lombana",
	 *				"Direccion": "Cl 29 #11-12",
	 *				"Telefono": "7212121",
	 *				"Clave": "sdlfkd$$$%",
	 *			},	
	 *			"Restaurante":{
	 *				"Id": "1",
	 *				"Nombre": "SushieLong",
	 *				"Nit" : "89448484",
	 *				"Direccion": "Avenida los estudiantes",
	 *				"Telefono1": "7212121",
	 *				"Telefono2": "7210099",	
	 *				"Celular": "300999888"	,
	 *				"ValorPedidoMinimo": "50000",	
	 *				"ValorDomicilio": "2500",
	 *				"PedidosOnLine": "true"	,
	 *				"PedidosTelefono": "true"	,
	 *				"EstadoRestaurante": "true"	,
	 *				"Latitud": "0.10"	,
	 *				"Longitud": "0.10"	,					
	 *			},
	 *			"DetallePedido":
	 *			[
	 *				{
	 *					"id":"1",
	 *					"Cantidad":"3",
	 *					"PorcentajePromo":"20",
	 *					"Precio":"30.000",
	 *					"Producto":
	 *					{
	 *						"Id":"1",
	 *						"Nombre":"Hamburguesa con pollo",
	 *						"DescrpcionCorta":"Hamburguesa de pollo papas y gaseosa",
	 *						"DescripcionLarga":"Deliciosa hamburguesa de pollo papas y gaseosa",
	 *						"Precio":"25000",
	 *						"PorcentajePromo":"30",
	 *						"TipoProducto":"P",
	 *					}
	 *				},
	 *				
	 *			]		
	 *		}]
	 *
	 * @apiError Parametros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	Insertar: function(req, res) {
		res.send('respond with a resource');
	},

	
	/**
	 * @api {get} /pedido/:id Consulta un pedido por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName CargarPorId
	 * @apiGroup PedidoController		
	 * @apiDescription Este método retorna un pedido por su Id
	 *
	 * @apiParam {Number} Id del pedido
	 *
	* @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 Ok
	 *		[{
	*			"Id": "1",
	 *			"Fecha": "2014-01-01 05:25 pm",
	 *			"Estado": "A",
	 *			"CalificacionUsuario": "5",
	 *			"ComentarioUsuario": "Estuvo bien.",
	 *			"ObservacionUsuario": "Poner mucha salsa de tomate",
	 *			"ObservacionEmpresa": "Atender bien al cliente",
	 *			"MedioPago":
	 *			{
	 *				"Id": "1",
	 *				"Nombre" "Efectivo",
	 *			},
	 *			"Usuario":{
	 *				"Id": "1",
	 *				"Correo": "criferlo@gmail.com",
	 *				"Nombre" : "Cristhian Lombana",
	 *				"Direccion": "Cl 29 #11-12",
	 *				"Telefono": "7212121",
	 *				"Clave": "sdlfkd$$$%",
	 *			},	
	 *			"Restaurante":{
	 *				"Id": "1",
	 *				"Nombre": "SushieLong",
	 *				"Nit" : "89448484",
	 *				"Direccion": "Avenida los estudiantes",
	 *				"Telefono1": "7212121",
	 *				"Telefono2": "7210099",	
	 *				"Celular": "300999888"	,
	 *				"ValorPedidoMinimo": "50000",	
	 *				"ValorDomicilio": "2500",
	 *				"PedidosOnLine": "true"	,
	 *				"PedidosTelefono": "true"	,
	 *				"EstadoRestaurante": "true"	,
	 *				"Latitud": "0.10"	,
	 *				"Longitud": "0.10"	,					
	 *			},
	 *			"DetallePedido":
	 *			[
	 *				{
	 *					"id":"1",
	 *					"Cantidad":"3",
	 *					"PorcentajePromo":"20",
	 *					"Precio":"30.000",
	 *					"Producto":
	 *					{
	 *						"Id":"1",
	 *						"Nombre":"Hamburguesa con pollo",
	 *						"DescrpcionCorta":"Hamburguesa de pollo papas y gaseosa",
	 *						"DescripcionLarga":"Deliciosa hamburguesa de pollo papas y gaseosa",
	 *						"Precio":"25000",
	 *						"PorcentajePromo":"30",
	 *						"TipoProducto":"P",
	 *					}
	 *				},
	 *				
	 *			]		
	 *		}]
	 * @apiError Pedido no encontrado
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existe pedido con el id {:id}"
 	 *     }
	 */
	CargarPorId: function(req, res) {
		res.send('respond with a resource');
	},

	/**
	 * @api {delete} /pedido/:id Elimina un pedido por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName EliminarPorId
	 * @apiGroup PedidoController
	 * @apiDescription Este método elimina una imagen por su Id
	 *
	 * @apiParam {Number} Id del pedido
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		{
	 *			
	 *		}
	 *
	 * @apiError Pedido con relación
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 412 Falló precondición
 	 *     {
 	 *       "error": "El pedido con el id {:id} se usa en una relación, no es posible eliminar"
 	 *     }
	 */
	EliminarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

	/**
	 * @api {patch} /pedido/:id Modifica un medio de pago en el sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName ModificarPorId
	 * @apiGroup PedidoController
	 * @apiDescription Este método modifica un pedido en el sistema
	 *
	 * @apiSuccess {Number} Id del pedido
	 * @apiSuccess {Date} Fecha  Fecha y hora cuando se hizo el pedido
	 * @apiSuccess {Enum} Estado 
	 * @apiSuccess {Number} CalificacionUsuario 
	 * @apiSuccess {String} ComentarioUsuario 
	 * @apiSuccess {String} ObservacionUsuario 
	 * @apiSuccess {String} ObservacionEmpresa 
	 * @apiSuccess {MedioPago} MedioPago  
	 * @apiSuccess {Usuario} Usuario
	 * @apiSuccess {Restaurante} Restaurante
	 * @apiSuccess {DetallePedido} DetallePedido  
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 Ok
	 *		[{
	*			"Id": "1",
	 *			"Fecha": "2014-01-01 05:25 pm",
	 *			"Estado": "A",
	 *			"CalificacionUsuario": "5",
	 *			"ComentarioUsuario": "Estuvo bien.",
	 *			"ObservacionUsuario": "Poner mucha salsa de tomate",
	 *			"ObservacionEmpresa": "Atender bien al cliente",
	 *			"MedioPago":
	 *			{
	 *				"Id": "1",
	 *				"Nombre" "Efectivo",
	 *			},
	 *			"Usuario":{
	 *				"Id": "1",
	 *				"Correo": "criferlo@gmail.com",
	 *				"Nombre" : "Cristhian Lombana",
	 *				"Direccion": "Cl 29 #11-12",
	 *				"Telefono": "7212121",
	 *				"Clave": "sdlfkd$$$%",
	 *			},	
	 *			"Restaurante":{
	 *				"Id": "1",
	 *				"Nombre": "SushieLong",
	 *				"Nit" : "89448484",
	 *				"Direccion": "Avenida los estudiantes",
	 *				"Telefono1": "7212121",
	 *				"Telefono2": "7210099",	
	 *				"Celular": "300999888"	,
	 *				"ValorPedidoMinimo": "50000",	
	 *				"ValorDomicilio": "2500",
	 *				"PedidosOnLine": "true"	,
	 *				"PedidosTelefono": "true"	,
	 *				"EstadoRestaurante": "true"	,
	 *				"Latitud": "0.10"	,
	 *				"Longitud": "0.10"	,					
	 *			},
	 *			"DetallePedido":
	 *			[
	 *				{
	 *					"id":"1",
	 *					"Cantidad":"3",
	 *					"PorcentajePromo":"20",
	 *					"Precio":"30.000",
	 *					"Producto":
	 *					{
	 *						"Id":"1",
	 *						"Nombre":"Hamburguesa con pollo",
	 *						"DescrpcionCorta":"Hamburguesa de pollo papas y gaseosa",
	 *						"DescripcionLarga":"Deliciosa hamburguesa de pollo papas y gaseosa",
	 *						"Precio":"25000",
	 *						"PorcentajePromo":"30",
	 *						"TipoProducto":"P",
	 *					}
	 *				},
	 *				
	 *			]		
	 *		}]
	 *
	 * @apiError Parametros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	ModificarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

};