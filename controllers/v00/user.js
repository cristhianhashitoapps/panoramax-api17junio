module.exports = {
	/**
	 * @api {get} /users Request for a list of users
	 * @apiVersion 0.0.0
	 *
	 * @apiName List
	 * @apiGroup User
	 * @apiDescription This method return a list of all users on the system
	 *
	 *
	 * @apiSuccess {String} firstname Firstname of the User.
	 * @apiSuccess {String} lastname  Lastname of the User.
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"username": "gedarufi",
	 *			"name": "Germán David Ruiz Figueroa"
	 *		}]
	 */
	Listar: function(req, res) {
		res.send('respond with a resource');
	},	
	/**
	 * @api {get} /users/:id Request for a especific user by id
	 * @apiVersion 0.0.0
	 *
	 * @apiName GetUser
	 * @apiGroup User
	 * @apiDescription This method return a especific user by id
	 *
	 * @apiParam {String} id Users unique ID.
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		{
	 *			"username": "gedarufi",
	 *			"name": "Germán David Ruiz Figueroa",
	 *			"contraseña": "123123213"
	 *		}
	 *
	 * @apiError UserNotFound The id of the User was not found.
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "La Encuesta con el id: {:id} no existe"
 	 *     }
	 */
	ObtenerUsuario: function(req, res) {

	}
};