var CategoriaMenu = require('../../repositories/CategoriaMenu');

module.exports = {
	/**
	 * @api {get} /categoriaMenu Consulta de todas las categorias del sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName ListarTodos
	 * @apiGroup CategoriaMenuController
	 * @apiDescription Este metodo retorna una lista de todas las categorias del sistema
	 *
	 *
	 * @apiSuccess {Number} Id de la categoria
	 * @apiSuccess {String} Nombre de la categoria
	 * @apiSuccess {String} NombreCorto  de la categoria
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Pollo asado"
	 *			"NombreCorto": "P.asado"
	 *		}]
	 * @apiError Categorias no encontradas
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existen categorías en el sistema actualmente"
 	 *     }
	 */
	ListarTodos: function(req, res) {
		//res.send('respond with a resource');
		CategoriaMenu.CargarCategorias(function(err, categorias){
			if(err){
				res.status(500).send(err);
			}else{
				res.send(categorias);	
			}
		});
	},	
	/**
	 * @api {put} /categoriaMenu/:id Inserta una categoria en el sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName Insertar
	 * @apiGroup CategoriaMenuController
	 * @apiDescription Este método inserta una categoria en el sistema
	 *
	 * @apiParam {String} Nombre de la categoria
	 * @apiParam {String} NombreCorto de la categoria
	 *
	* @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 201 Creado
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Pollo asado"
	 *			"NombreCorto": "P.asado"
	 *		}]
	 *
	 * @apiError Parametros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	Insertar: function(req, res) {
		CategoriaMenu.InsertarCategoria(req.body, function(err, categoria){
			if(err){
				res.status(500).send(err);
			}else{
				res.send(categoria);	
			}
		});
	},

	/**
	 * @api {get} /categoriaMenu/:id Consulta una categoría por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName CargarPorId
	 * @apiGroup CategoriaMenuController
	 * @apiDescription Este método retorna una categoría por su Id
	 *
	 * @apiParam {Number} Id de la categoría
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Pollo asado"
	 *			"NombreCorto": "P.asado"
	 *		}]
	 * @apiError Categoria no encontrada
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existen categoría con el id {:id}"
 	 *     }
	 */
	CargarPorId: function(req, res) {
		res.send('respond with a resource');
	},

	/**
	 * @api {delete} /categoriaMenu/:id Elimina una categoria por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName EliminarPorId
	 * @apiGroup CategoriaMenuController
	 * @apiDescription Este método elimina una categoría por su Id
	 *
	 * @apiParam {Number} Id de la categoría
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		{
	 *			
	 *		}
	 *
	 * @apiError Categoría con relación
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 412 Falló precondición
 	 *     {
 	 *       "error": "La categoría con el id {:id} se usa en una relación, no es posible eliminar"
 	 *     }
	 */
	EliminarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

	/**
	 * @api {patch} /categoriaMenu/:id Modifica una categoría por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName ModificarPorId
	 * @apiGroup CategoriaMenuController
	 * @apiDescription Este método modifica una categoría por su Id
	 *
	 * @apiParam {Number} Id de la categoría
	 * @apiParam {String} Nombre de la categoría
	 * @apiParam {String} NombreCorto de la categoría
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"Nombre": "Pollo asado"
	 *			"NombreCorto": "P.asado modificado"
	 *		}]
	 *
 	 * @apiError Parámetros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	ModificarPorId: function(req, res) {
		res.send('respond with a resource');	
	},
};