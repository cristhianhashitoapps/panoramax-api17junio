module.exports = {
	
	/**
	 * @api {get} /usuario Consulta de todos los usuarios del sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName ListarTodos
	 * @apiGroup UsuarioController
	 * @apiDescription Este metodo retorna una lista de todos los usuarios del sistema
	 *
	 *
	 * @apiSuccess {Number} Id del usuario
	 * @apiSuccess {String} Correo del usuario
	 * @apiSuccess {String} Nombre  del usuario
	 * @apiSuccess {Direccion} Direccion  del usuario
	 * @apiSuccess {Telefono} Telefono  del usuario
	 * @apiSuccess {Clave} Clave del usuario
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	  *				"Id": "1",
	 *				"Correo": "criferlo@gmail.com",
	 *				"Nombre" : "Cristhian Lombana",
	 *				"Direccion": "Cl 29 #11-12",
	 *				"Telefono": "7212121",
	 *				"Clave": "sdlfkd$$$%",
	 *		}]
	 * @apiError Usuarios no encontrados
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existen usuarios en el sistema actualmente"
 	 *     }
	 *

	 */
	ListarTodos: function(req, res) {
		res.send('respond with a resource');
	},	

	/**
	 * @api {put} /usuario/:id Inserta un usuario en el sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName Insertar
	 * @apiGroup UsuarioController
	 * @apiDescription Este método inserta un usuario en el sistema
	 *
	 * @apiParam {Number} Id del usuario
	 * @apiParam {String} Correo del usuario
	 * @apiParam {String} Nombre  del usuario
	 * @apiParam {Direccion} Direccion  del usuario
	 * @apiParam {Telefono} Telefono  del usuario
	 * @apiParam {Clave} Clave del usuario
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 201 Creado
	 *		[{
	 *				"Id": "1",
	 *				"Correo": "criferlo@gmail.com",
	 *				"Nombre" : "Cristhian Lombana",
	 *				"Direccion": "Cl 29 #11-12",
	 *				"Telefono": "7212121",
	 *				"Clave": "sdlfkd$$$%",
	 *		}]
	 *
	 * @apiError Parametros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	Insertar: function(req, res) {
		res.send('respond with a resource');
	},

	
	/**
	 * @api {get} /usuario/:id Consulta un usuario en el sistema por Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName CargarPorId
	 * @apiGroup UsuarioController
	 * @apiDescription Este metodo retorna una lista de todos los usuarios del sistema
	 *
	 *
	 * @apiSuccess {Number} Id del usuario
	 * @apiSuccess {String} Correo del usuario
	 * @apiSuccess {String} Nombre  del usuario
	 * @apiSuccess {Direccion} Direccion  del usuario
	 * @apiSuccess {Telefono} Telefono  del usuario
	 * @apiSuccess {Clave} Clave del usuario
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	  *				"Id": "1",
	 *				"Correo": "criferlo@gmail.com",
	 *				"Nombre" : "Cristhian Lombana",
	 *				"Direccion": "Cl 29 #11-12",
	 *				"Telefono": "7212121",
	 *				"Clave": "sdlfkd$$$%",
	 *		}]
	 * @apiError Usuarios no encontrado
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *        "error": "No existen usuario con el id {:id}"
 	 *     }
	 *

	 */
	CargarPorId: function(req, res) {
		res.send('respond with a resource');
	},

	
	/**
	 * @api {delete} /usuario/:id Elimina un usuario por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName EliminarPorId
	 * @apiGroup UsuarioController
	 * @apiDescription Este método elimina un usuario por su Id
	 *
	 * @apiParam {Number} Id del usuario
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		{
	 *			
	 *		}
	 *
	 * @apiError Usuario con relación
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 412 Falló precondición
 	 *     {
 	 *       "error": "El usuario con el id {:id} se usa en una relación, no es posible eliminar"
 	 *     }
	 */
	EliminarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

	/**
	 * @api {patch} /usuario/:id Modifica un usuario en el sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName ModificarPorId
	 * @apiGroup UsuarioController
	 * @apiDescription Este método modifica un usuario en el sistema
	 *
	 * @apiParam {Number} Id del usuario
	 * @apiParam {String} Correo del usuario
	 * @apiParam {String} Nombre  del usuario
	 * @apiParam {Direccion} Direccion  del usuario
	 * @apiParam {Telefono} Telefono  del usuario
	 * @apiParam {Clave} Clave del usuario
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 Ok
	 *		[{
	 *				"Id": "1",
	 *				"Correo": "criferlo@gmail.com",
	 *				"Nombre" : "Cristhian Lombana",
	 *				"Direccion": "Cl 29 #11-12",
	 *				"Telefono": "7212121",
	 *				"Clave": "sdlfkd$$$%",
	 *		}]
	 *
	 * @apiError Parametros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	ModificarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

};