module.exports = {
	/**
	 * @api {get} /imagen Consulta de todas las imagenes del sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName ListarTodos
	 * @apiGroup ImagenController
	 * @apiDescription Este metodo retorna una lista de todas las imagenes del sistema
	 *
	 *
	 * @apiSuccess {Number} Id de la imagen
	 * @apiSuccess {String} RutaGrande Ruta completa de la imagen grande
	 * @apiSuccess {String} RutaPequeno  Ruta completa de la imagen pequeña
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"RutaGrande": "http://servidor/app/img.jpg",
	 *			"RutaPequeno": "http://servidor/app/img.jpg"
	 *		}]
	 * @apiError Imagenes no encontradas
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existen imagenes en el sistema actualmente"
 	 *     }
	 *

	 */
	ListarTodos: function(req, res) {
		res.send('respond with a resource');
	},	
	
	/**
	 * @api {put} /imagen/:id Inserta una imagen en el sistema
	 * @apiVersion 0.0.0
	 *
	 * @apiName Insertar
	 * @apiGroup ImagenController
	 * @apiDescription Este método inserta una imagen en el sistema
	 *
	 * @apiParam {String} RutaGrande de la imagen
	 * @apiParam {String} RutaPequeno de la imagen
	 *
	* @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 201 Creado
	 *		[{
	 *			"Id": "1",
	 *			"RutaGrande": "http://servidor/app/img.jpg"
	 *			"RutaPequeno": "http://servidor/app/img.jpg"
	 *		}]
	 *
	 * @apiError Parametros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	Insertar: function(req, res) {
		res.send('respond with a resource');
	},

	/**
	 * @api {get} /imagen/:id Consulta una categoría por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName CargarPorId
	 * @apiGroup ImagenController
	 * @apiDescription Este método retorna una imagen por su Id
	 *
	 * @apiParam {Number} Id de la imagen
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"RutaGrande": "http://servidor/app/img.jpg"
	 *			"RutaPequeno": "http://servidor/app/img.jpg"
	 *		}]
	 * @apiError Imagen no encontrada
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "No existen imagen con el id {:id}"
 	 *     }
	 */
	CargarPorId: function(req, res) {
		res.send('respond with a resource');
	},

	/**
	 * @api {delete} /imagen/:id Elimina una imagen por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName EliminarPorId
	 * @apiGroup ImagenController
	 * @apiDescription Este método elimina una imagen por su Id
	 *
	 * @apiParam {Number} Id de la imagen
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		{
	 *			
	 *		}
	 *
	 * @apiError Imagen con relación
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 412 Falló precondición
 	 *     {
 	 *       "error": "La imagen con el id {:id} se usa en una relación, no es posible eliminar"
 	 *     }
	 */
	EliminarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

	
	/**
	 * @api {patch} /imagen/:id Modifica una imagen por su Id
	 * @apiVersion 0.0.0
	 *
	 * @apiName ModificarPorId
	 * @apiGroup ImagenController
	 * @apiDescription Este método modifica una imagen por su Id
	 *
	 * @apiParam {Number} Id de la imagen
	 * @apiParam {String} Nombre de la categoría
	 * @apiParam {String} NombreCorto de la categoría
	 *
	 * @apiSuccessExample Success-Response:
	 * 		HTTP/1.1 200 OK
	 *		[{
	 *			"Id": "1",
	 *			"RutaGrande": "http://servidor/app/img.jpg"
	 *			"RutaPequeno": "http://servidor/app/img.jpg"
	 *		}]
	 *
 	 * @apiError Parámetros incorrectos 
	 *
 	 * @apiErrorExample Error-Response:
 	 *     HTTP/1.1 400 Solicitud incorrecta
 	 *     {
 	 *       "error": "Uno de los parámetros es incorrecto o falta"
 	 *     }
	 */
	ModificarPorId: function(req, res) {
		res.send('respond with a resource');	
	},

};