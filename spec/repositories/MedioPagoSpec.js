describe('MedioPagoSpec', function() {
  process.env.NODE_ENV = 'test';

  var repo    = require('../../repositories/MedioPago');
  var models  = require('../../models');
  var Promise = require('bluebird');

  beforeAll(function(done) {
    models
      .sequelize
      .sync({
        force: true,
        logging: false
      }).then(function() {
        console.log('Se sincronizo el modelo');
        done();
      });
  });

  describe('Lógica de negocio', function() {
    //Hay que implementar los test para la lógica de negocio
  });

  describe('CRUD básico', function() {
    var MedioId;

    it('Insertar MedioPago', function(done) {
      var medio = {
        Nombre: 'Entradas'
      };

      repo
        .Insert(medio)
        .then(function(medio) {
          expect(medio).not.toBeNull();

          // Obtengo el Id del modelo insertado para usarlo
          // en las siguientes pruebas
          MedioId = medio.Id;

          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();

          done();
        });
    });

    it('Cargar MedioPago x Id', function(done) {
      console.log(MedioId);
      //Se usa el Id Obtenido en  el proceso se inserción
      repo
        .LoadById(MedioId)
        .then(function(medio) {
          expect(medio).not.toBeNull();
          expect(medio.Nombre).toEqual('Entradas');
          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();

          done();
        });
    });

    it('Listar MedioPago', function(done) {
      repo
        .LoadPage(1, 100, 'Nombre', 'ASC')
        .then(function(medios) {
          expect(medios).not.toBe(undefined);
          expect(medios.length).toBeGreaterThan(0);
          done();
        })
        .catch(function(error) {
          expect(err).toBeNull();
          done();
        });
    });

    it('Actualizar MedioPago', function(done) {
      var nvoMedio = {
        Nombre: 'Entradas Actualizadas'
      };

      repo
        .Update(MedioId, nvoMedio)
        .then(function(medio) {
          expect(medio).not.toBeNull();
          expect(medio.Nombre).toEqual('Entradas Actualizadas');

          done();
        })
        .catch(function(error) {
          expect(err).toBeNull();
          done();
        });
    });

    it('Eliminar MedioPago', function(done) {
      repo
        .Delete(MedioId)
        .then(function(medio) {
          return repo.LoadById(MedioId);
        })
        .then(function(medio) {
          expect(medio).toBeNull();
          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();
          done();
        });
    });
  });
});
