describe('ProductoSpec', function() {
  process.env.NODE_ENV = 'test';

  var models  = require('../../models');
  var repo    = require('../../repositories/Producto');

  beforeAll(function(done) {
    models
      .sequelize
      .sync({
        force: true,
        logging: false
      }).then(function() {
        console.log('Se sincronizó el modelo');
        done();
      });
  });

  describe('CRUD básico con herencia', function() {    
    var ProductoId;
    var producto = {
      Nombre: 'PAPAS MARGARITA NUEVA SAL',
      DescripcionCorta: 'NUEVAS PAPAS MARGARITA SAL',
      DescripcionLarga: 'NUEVAS PAPAS MARGARITA CON SAL Y PIMIENTA',
      Precio: 3000.23,
      PorcentajePromo: 4.2,
      TipoProducto: 'P'
    };

    it('Insertar Producto', function(done) {
      repo
        .Insert(producto)
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.Nombre).toEqual(producto.Nombre);
          expect(model.DescripcionCorta).toEqual(producto.DescripcionCorta);
          expect(model.DescripcionLarga).toEqual(producto.DescripcionLarga);
          expect(model.Precio).toEqual(producto.Precio);
          expect(model.PorcentajePromo).toEqual(producto.PorcentajePromo);
          expect(model.TipoProducto).toEqual(producto.TipoProducto);

          ProductoId = model.Id;

          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();
          done();
        });
    });

    it('Cargar Producto x Id', function(done) {
      repo
        .LoadById(ProductoId)
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.Nombre).toEqual(producto.Nombre);
          expect(model.DescripcionCorta).toEqual(producto.DescripcionCorta);
          expect(model.DescripcionLarga).toEqual(producto.DescripcionLarga);
          expect(model.Precio).toEqual(producto.Precio);
          expect(model.PorcentajePromo).toEqual(producto.PorcentajePromo);
          expect(model.TipoProducto).toEqual(producto.TipoProducto);

          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();

          done();
        })
    });

    it('Actualizar Producto', function(done) {
      repo
        .Update(ProductoId, {Nombre: 'PAPAS MARGARITA NUEVA SAL X 2'})
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.Nombre).toEqual('PAPAS MARGARITA NUEVA SAL X 2');
          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();
          done();
        });
    });

    it('Eliminar Producto', function(done) {
      repo
        .Delete(ProductoId)
        .then(function(model) {
          return repo.LoadById(ProductoId);
        })
        .then(function(model) {
          expect(model).toBeNull();
          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();
          done();
        });
    })
  });
});
