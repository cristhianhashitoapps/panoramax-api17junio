describe('ImagenSpec', function() {
  process.env.NODE_ENV = 'test';

  var models = require('../../models');
  var repo   = require('../../repositories/Imagen');

  beforeAll(function(done) {
    models
      .sequelize
      .sync({
        force: true,
        logging: false
      }).then(function() {
        console.log('Se sincronizó el modelo');
        done();
      });
  });

  describe('CRUD básico con herencia', function() {
    var ImagenId;
    var imagen = {
      RutaGrande: '/var/www/img/1.jpg',
      RutaPequeño: '/var/www/img/1p.jpg',
    };

    it('Insertar Imagen', function(done) {
      repo
        .Insert(imagen)
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.RutaGrande).toEqual(imagen.RutaGrande);
          expect(model.RutaPequeño).toEqual(imagen.RutaPequeño);

          ImagenId = model.Id;

          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();
          done();
        });
    });

    it('Cargar Imagen x Id', function(done) {
      repo
        .LoadById(ImagenId)
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.RutaGrande).toEqual(imagen.RutaGrande);
          expect(model.RutaPequeño).toEqual(imagen.RutaPequeño);

          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();

          done();
        })
    });

    it('Actualizar Imagen', function(done) {
      repo
        .Update(ImagenId, {RutaGrande: '/var/www/img/2.jpg'})
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.RutaGrande).toEqual('/var/www/img/2.jpg');
          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();
          done();
        });
    });

    it('Eliminar Direccion', function(done) {
      repo
        .Delete(ImagenId)
        .then(function(model) {
          return repo.LoadById(ImagenId);
        })
        .then(function(model) {
          expect(model).toBeNull();
          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();
          done();
        });
    })
  });
});
