describe('VariedadSpec', function() {
  process.env.NODE_ENV = 'test';

  var repo    = require('../../repositories/Variedad');
  var models  = require('../../models');
  var Promise = require('bluebird');

  beforeAll(function(done) {
    models
      .sequelize
      .sync({
        force: true,
        logging: false
      }).then(function() {
        console.log('Se sincronizo el modelo');
        done();
      });
  });

  describe('Lógica de negocio', function() {
    it('Valiación para nuevas categorias', function() {
      var categoria = {};

      expect(repo.Validate(categoria)).not.toBeNull();

      categoria.Nombre = 'Nombre Categoría';

      expect(repo.Validate(categoria)).toBeNull();
    });
  });

  describe('CRUD básico', function() {
    var CategoriaId;
    var categoriaNva = {
      Nombre: 'Entradas',
    };

    it('Insertar Variedad', function(done) {
      repo
        .Insert(categoriaNva)
        .then(function(categoria) {
          expect(categoria).not.toBeNull();

          // Obtengo el Id del modelo insertado para usarlo
          // en las siguientes pruebas
          CategoriaId = categoria.Id;

          done();
        })
        .catch(function(err) {
          expect(err).toBeNull();

          done();
        });
    });

    it('Cargar Variedad x Id', function(done) {
      //Se usa el Id Obtenido en  el proceso se inserción
      repo
        .LoadById(CategoriaId)
        .then(function(categoria) {
          expect(categoria).not.toBeNull();
          expect(categoria.Nombre).toEqual(categoriaNva.Nombre);

          done();
        })
        .catch(function(err) {
          expect(err).toBeNull();

          done();
        });
    });

    it('Actualizar Variedad', function(done) {
      var categoriaUpt = {
        Nombre: 'Entradas Actualizadas',
        NombreCorto: 'ENT_ACT'
      };

      repo
        .Update(CategoriaId, categoriaUpt)
        .then(function(categoria) {
          expect(categoria).not.toBeNull();
          expect(categoria.Nombre).toEqual(categoriaUpt.Nombre);

          done();
        })
        .catch(function(err) {
          expect(err).toBeNull();

          done();
        });
    });

    it('Eliminar Variedad', function(done) {
      repo
        .Delete(CategoriaId)
        .then(function(categoria) {
          return repo.LoadById(CategoriaId);
        })
        .then(function(categoria) {
          expect(categoria).toBeNull();

          done();
        })
        .catch(function(err) {
          expect(err).toBeNull();

          done();
        })
    });

    it('Listar Variedad', function(done) {
      var categorias    = [];
      var page          = 1;
      var pageSize      = 20;
      var orderBy       = 'Nombre';
      var sortOrder     = 'ASC';
      var totalItems    = 26;
      var categoriasIns = [];

      for (var i = 0; i < totalItems; i++) {
        categorias.push({
          Nombre: ['Categoria', Math.random()].join(' '),
          NombreCorto: ['CAT', Math.random()].join('_'),
          Restaurante: {}
        });
      };

      Promise
        .map(categorias, function(categoria) {
          return repo.Insert(categoria);
        })
        .then(function(categorias) {
          expect(categorias.length).toEqual(totalItems);

          categoriasIns = categorias;

          return repo.LoadPage(page, pageSize, orderBy, sortOrder);
        })
        .then(function(categorias) {
          expect(categorias.length).toEqual(pageSize);

          return repo.LoadPage(page + 1, pageSize, orderBy, sortOrder);
        })
        .then(function(categorias) {
          expect(categorias.length).toEqual(totalItems - pageSize);
        })
        .then(function() {
          return Promise.map(categoriasIns, function(categoria) {
              return repo.Delete(categoria.Id);
            });
        })
        .then(function() {
          done();
        })
        .catch(function(err) {
          expect(err).toBeNull();

          done();
        });
    });
  });
});
