describe('UsuarioSpec', function() {
  process.env.NODE_ENV = 'test';

  var models  = require('../../models');
  var repo    = require('../../repositories/Usuario');

  beforeAll(function(done) {
    models
      .sequelize
      .sync({
        force: true,
        logging: false
      }).then(function() {
        console.log('Se sincronizó el modelo');
        done();
      });
  });

  describe('CRUD básico con herencia', function() {
    var UsuarioId;
    var usuario = {
      Correo: 'gedarufi@hotmail.com',
      Nombre: 'Germán David Ruiz Figueroa',
      Clave: '12345678'
    };

    it('Insertar Usuario', function(done) {
      repo
        .Insert(usuario)
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.Correo).toEqual(usuario.Correo);
          expect(model.Nombre).toEqual(usuario.Nombre);
          expect(model.Clave).toEqual(usuario.Clave);

          UsuarioId = model.Id;

          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();
          done();
        });
    });

    it('Cargar Usuario x Id', function(done) {
      repo
        .LoadById(UsuarioId)
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.Correo).toEqual(usuario.Correo);
          expect(model.Nombre).toEqual(usuario.Nombre);
          expect(model.Clave).toEqual(usuario.Clave);

          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();

          done();
        })
    });

    it('Actualizar Usuario', function(done) {
      repo
        .Update(UsuarioId, {Direccion: 'Pucalpa 3 Bq 15b Apto 301'})
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.Direccion).toEqual('Pucalpa 3 Bq 15b Apto 301');
          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();
          done();
        });
    });

    it('Eliminar Usuario', function(done) {
      repo
        .Delete(UsuarioId)
        .then(function(model) {
          return repo.LoadById(UsuarioId);
        })
        .then(function(model) {
          expect(model).toBeNull();
          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();
          done();
        });
    })
  });
});
