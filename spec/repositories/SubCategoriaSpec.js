describe('SubCategoriaSpec', function() {
  process.env.NODE_ENV = 'test';

  var repo    = require('../../repositories/SubCategoria');
  var models  = require('../../models');
  var Promise = require('bluebird');

  beforeAll(function(done) {
    models
      .sequelize
      .sync({
        force: true,
        logging: false
      }).then(function() {
        console.log('Se sincronizo el modelo');
        done();
      });
  });

  describe('Lógica de negocio', function() {
    //Hay que implementar los test para la lógica de negocio
  });

  describe('CRUD básico', function() {
    var SubCategoriaId;

    it('Insertar SubCategoria', function(done) {
      var SubCategoria = {
        Nombre: 'Camisetas'
      };

      repo
        .Insert(SubCategoria)
        .then(function(model) {
          SubCategoriaId = model.Id;

          expect(model.Id).not.toBeNull();
          expect(model.Nombre).not.toBeNull();
          expect(model.Nombre).toEqual(SubCategoria.Nombre);

          done();
        })
        .catch(function(error) {
          expect(error)
            .toEqual('Debe de proporcionar un nombre de sub-cateogría valido');

          done();
        });
    });

    it('Cargar SubCategoria x Id', function(done) {
      //Se usa el Id Obtenido en  el proceso se inserción
      repo
        .LoadById(SubCategoriaId)
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.Nombre).toEqual('Camisetas');

          done();
        })
        .catch(function(error) {
          expect(err).toBeNull();

          done();
        });
    });

    it('Actualizar SubCategoria', function(done) {
      var nvoSubCategoria = {
        Nombre: 'Entradas Actualizadas',
      };

      repo
        .Update(SubCategoriaId, nvoSubCategoria)
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.Nombre).toEqual(nvoSubCategoria.Nombre);

          done();
        })
        .catch(function(err) {
            expect(err).toBeNull();

            done();
          });
    });

    it('Eliminar SubCategoria', function(done) {
      repo
        .Delete(SubCategoriaId)
        .then(function(subcategoria) {
          return repo.LoadById(SubCategoriaId);
        })
        .then(function(subcategoria) {
          expect(subcategoria).toBeNull();

          done();
        })
        .catch(function(err) {
          expect(err).toBeNull();

          done();
        });
    });

    it('Listar SubCategoria', function(done) {
      var subcategorias    = [];
      var page             = 1;
      var pageSize         = 20;
      var orderBy          = 'Nombre';
      var sortOrder        = 'ASC';
      var totalItems       = 26;
      var subcategoriasIns = [];

      for (var i = 0; i < totalItems; i++) {
        subcategorias.push({
          Nombre: ['Subcategoria', Math.random()].join(' ')
        });
      }

      Promise
        .map(subcategorias, function(sub) {
          return repo.Insert(sub);
        })
        .then(function(subcategorias) {
          expect(subcategorias.length).toEqual(totalItems);

          subcategoriasIns = subcategorias;

          return repo.LoadPage(page, pageSize, orderBy, sortOrder);
        })
        .then(function(subcategorias) {
          expect(subcategorias.length).toEqual(pageSize);

          return repo.LoadPage(page + 1, pageSize, orderBy, sortOrder);
        })
        .then(function(subcategorias) {
          expect(subcategorias.length).toEqual(totalItems - pageSize);
        })
        .then(function() {
          return Promise.map(subcategoriasIns, function(sub) {
              return repo.Delete(sub.Id);
            });
        })
        .then(function() {
          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();

          done();
        })
    });
  });
});
