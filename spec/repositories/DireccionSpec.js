describe('DireccionSpec', function() {
  process.env.NODE_ENV = 'test';

  var models = require('../../models');
  var repo   = require('../../repositories/Direccion');

  beforeAll(function(done) {
    models
      .sequelize
      .sync({
        force: true,
        logging: false
      }).then(function() {
        console.log('Se sincronizó el modelo');
        done();
      });
  });

  describe('CRUD básico con herencia', function() {
    var DireccionId;
    var direccion = {
      Linea1: 'Cra 7 1-149 Cajica',
      Linea2: 'Cra 7 1-149 Bogota',
      Referencia: 'Centro Cajica',
      Latitud: 15,
      Longitud: 14,
    };

    it('Insertar Direccion', function(done) {
      repo
        .Insert(direccion)
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.Linea1).toEqual(direccion.Linea1);
          expect(model.Linea2).toEqual(direccion.Linea2);
          expect(model.Referencia).toEqual(direccion.Referencia);
          expect(model.Latitud).toEqual(direccion.Latitud);
          expect(model.Longitud).toEqual(direccion.Longitud);
          DireccionId = model.Id;

          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();
          done();
        });
    });

    it('Cargar Direccion x Id', function(done) {
      repo
        .LoadById(DireccionId)
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.Linea1).toEqual(direccion.Linea1);
          expect(model.Linea2).toEqual(direccion.Linea2);
          expect(model.Referencia).toEqual(direccion.Referencia);
          expect(model.Latitud).toEqual(direccion.Latitud);
          expect(model.Longitud).toEqual(direccion.Longitud);

          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();

          done();
        })
    });

    it('Actualizar Direccion', function(done) {
      repo
        .Update(DireccionId, {Linea1: 'Cra 8 Bogota'})
        .then(function(model) {
          expect(model).not.toBeNull();
          expect(model.Linea1).toEqual('Cra 8 Bogota');
          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();
          done();
        });
    });

    it('Eliminar Direccion', function(done) {
      repo
        .Delete(DireccionId)
        .then(function(model) {
          return repo.LoadById(DireccionId);
        })
        .then(function(model) {
          expect(model).toBeNull();
          done();
        })
        .catch(function(error) {
          expect(error).toBeNull();
          done();
        });
    })
  });
});
