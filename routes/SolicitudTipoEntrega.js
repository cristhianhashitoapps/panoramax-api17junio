module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/SolicitudTipoEntrega',
    controllers.SolicitudTipoEntrega.GetAll);
  router.get('/SolicitudTipoEntrega/:Id',
    controllers.SolicitudTipoEntrega.GetById);
  router.post('/SolicitudTipoEntrega',
    controllers.SolicitudTipoEntrega.InsertarSolicitudTipoEntrega);
  router.put('/SolicitudTipoEntrega/:Id',
    controllers.SolicitudTipoEntrega.Update);
  router.delete('/SolicitudTipoEntrega/:Id',
    controllers.SolicitudTipoEntrega.Delete);

  return router;
};
