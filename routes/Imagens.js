module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/Imagens',
    controllers.Imagens.GetAll);
  router.get('/Imagens/:Id',
    controllers.Imagens.GetById);
  router.post('/Imagens',
    controllers.Imagens.Insert);
  router.put('/Imagens/:Id',
    controllers.Imagens.Update);
  router.delete('/Imagens/:Id',
    controllers.Imagens.Delete);
  router.get('/Banners',
    controllers.Imagens.GetBanners);

  return router;
};
