module.exports = function(apiVersion,io) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var Repository = require('../repositories/Repository');
  var models     = require('../models');
  var ionicPushServer = require('ionic-push-server');
  /*var nodemailer    = require('nodemailer');

  var smtpTransport = require('nodemailer-smtp-transport');
  var transporter = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    auth: {
        user: 'panoramaxapp2017@gmail.com', // my mail
        pass: '2017apppanoramax'
    }
}));*/

  var includes   = [{
    model: models.Categoria
  }];
  var repository = Repository(models.SubCategoria, includes);

  console.log("io-------",io);

    io.on('connection', function(socket) {
    console.log('****************************************************');
    console.log('****************************************************');
    console.log('A new user connected');
    console.log('****************************************************');
    console.log('****************************************************');

    socket.on('disconnect', function() {
      console.log('User disconnected');
    });
    socket.on('backend', function(msg) {
      console.log('message: ' + msg);
      io.emit('mobile', msg);
    });

    socket.on('imprimir-pedido',function(msg){
      io.emit('new-order', msg);
      console.log("nuevo pedido",msg);
    });
  });

  function ReportOrder(req, res) {
    console.log("---datosssss",req.body,"----fin datoss");

    console.log("objeto para impresion --------"+req.body);

    
    io.emit('new-order', req.body);

    /*console.log("----------------------------enviando correo------------------------");

    var mailOptions = {
        from: 'Panoramax Ltda. <info@panoramaxltda.net>',
        to: 'criferlo@hotmail.com',
        subject: 'Confirmación orden: '
      };

      //mailOptions.text = body.Message;
      mailOptions.html = "Aquí va el correo";

      transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
          console.log("Error enviando mensaje: "+error);
        } else {
          console.log('Message sent: ' + info.response);
        }
      });

    console.log("---------------------------- fin enviando correo------------------------");*/

    res.send(req.pedido);



  };

  router.get('/SolicitudExamenes/darfecha',
    controllers.SolicitudExamenes.DarFecha);



  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/SolicitudExamenes',
    controllers.SolicitudExamenes.GetAll);
  router.get('/SolicitudExamenes/:Id',
    controllers.SolicitudExamenes.GetById);
  router.post('/SolicitudExamenes',
    controllers.SolicitudExamenes.Insert, ReportOrder);
  router.put('/SolicitudExamenes/:Id',
    controllers.SolicitudExamenes.Update);
  router.delete('/SolicitudExamenes/:Id',
    controllers.SolicitudExamenes.Delete);




  return router;
};
