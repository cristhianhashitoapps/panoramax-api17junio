module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/Paciente',
    controllers.Paciente.GetAll);
  router.get('/Paciente/:Id',
    controllers.Paciente.GetById);
  router.post('/Paciente',
    controllers.Paciente.Insert);
  router.put('/Paciente/:Id',
    controllers.Paciente.Update);
  router.delete('/Paciente/:Id',
    controllers.Paciente.Delete);
  router.get('/Paciente/:nombre/likename',
    controllers.Paciente.GetAllLikeName);

  return router;
};
