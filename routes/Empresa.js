module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/Empresa',
    controllers.Empresa.GetAll);
  router.get('/Empresa/:Id',
    controllers.Empresa.GetById);
  router.post('/Empresa',
    controllers.Empresa.Insert);
  router.put('/Empresa/:Id',
    controllers.Empresa.Update);
  router.delete('/Empresa/:Id',
    controllers.Empresa.Delete);

  return router;
};
