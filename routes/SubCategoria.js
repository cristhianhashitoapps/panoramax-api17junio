module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/SubCategoria',
    controllers.SubCategoria.GetAll);
  router.get('/SubCategoria/:Id',
    controllers.SubCategoria.GetById);
  router.post('/SubCategoria',
    controllers.SubCategoria.Insert);
  router.put('/SubCategoria/:Id',
    controllers.SubCategoria.Update);
  router.delete('/SubCategoria/:Id',
    controllers.SubCategoria.Delete);

  return router;
};
