module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/categorias',
    controllers.Categoria.GetAll);
  router.get('/categorias/:Id',
    controllers.Categoria.GetById);
  router.post('/categorias',
    controllers.Categoria.Insert);
  router.put('/categorias/:Id',
    controllers.Categoria.Update);
  router.delete('/categorias/:Id',
    controllers.Categoria.Delete);
  router.get('/categorias/:Id/bytiposervicioid',
      controllers.Categoria.CargarPorTipoServicioId);
  return router;
};
