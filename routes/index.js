/* Routes Configuration */
module.exports = function(app, server) {
  var io = require('socket.io')(server);


  /* Declaración de las diferentes rutas que se van a usar en el API */
  app.use('/api/v00',   require('./Categoria')('v00a'));
  app.use('/api/v00',   require('./DetallesExamenes')('v00a'));
  app.use('/api/v00',   require('./Doctor')('v00a'));
  app.use('/api/v00',   require('./Empresa')('v00a'));
  app.use('/api/v00',   require('./Imagens')('v00a'));

  app.use('/api/v00',   require('./Paciente')('v00a'));
  app.use('/api/v00',   require('./SolicitudExamenes')('v00a',io));
  app.use('/api/v00',   require('./SolicitudTipoEntrega')('v00a'));
  app.use('/api/v00',   require('./SubCategoria')('v00a'));
  app.use('/api/v00',   require('./TipoEntregaResultados')('v00a'));
  app.use('/api/v00',   require('./TipoEstado')('v00a'));
  app.use('/api/v00',   require('./TipoServicio')('v00a'));
  app.use('/api/v00',   require('./UsuarioBack')('v00a'));

};
