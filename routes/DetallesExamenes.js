module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/DetallesExamenes',
    controllers.DetallesExamenes.GetAll);
  router.get('/DetallesExamenes/:Id',
    controllers.DetallesExamenes.GetById);
  router.post('/DetallesExamenes',
    controllers.DetallesExamenes.Insert);
  router.put('/DetallesExamenes/:Id',
    controllers.DetallesExamenes.Update);
  router.delete('/DetallesExamenes/:Id',
    controllers.DetallesExamenes.Delete);
  router.post('/DetallesExamenes/InsertarDetallePedido',
    controllers.DetallesExamenes.InsertarDetallePedido);
  router.get('/DetallesExamenes/:Id/CargarDetallesPorSolicitudExamen',
    controllers.DetallesExamenes.CargarDetallesPorSolicitudExamen)

  return router;
};
