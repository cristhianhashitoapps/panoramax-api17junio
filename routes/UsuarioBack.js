module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer el API
   */
  router.get('/usuarios-back',
    controllers.UsuarioBack.GetAll);
  router.get('/usuarios-back/:Id',
    controllers.UsuarioBack.GetById);
  router.post('/usuarios-back',
    controllers.UsuarioBack.Insert);
  router.post('/usuarios-back/login',
    controllers.UsuarioBack.Login);
  router.put('/usuarios-back/:Id',
    controllers.UsuarioBack.Update);
  router.delete('/usuarios-back/:Id',
    controllers.UsuarioBack.Delete);

  return router;
};
