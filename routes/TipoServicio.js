module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/TipoServicio',
    controllers.TipoServicio.GetAll);
  router.get('/TipoServicio/:Id',
    controllers.TipoServicio.GetById);
  router.post('/TipoServicio',
    controllers.TipoServicio.Insert);
  router.put('/TipoServicio/:Id',
    controllers.TipoServicio.Update);
  router.delete('/TipoServicio/:Id',
    controllers.TipoServicio.Delete);

  return router;
};
