module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/Doctor',
    controllers.Doctor.GetAll);
  router.get('/Doctor/:Id',
    controllers.Doctor.GetById);
  router.get('/Doctor/:Id/Empresa',
    controllers.Doctor.GetEmpresas);
  router.post('/Doctor',
    controllers.Doctor.Insert);
  router.put('/Doctor/:Id',
    controllers.Doctor.Update);
  router.delete('/Doctor/:Id',
    controllers.Doctor.Delete);
  router.post('/SignUp',
      controllers.Doctor.SignUp);
  router.post('/Doctor/login',
        controllers.Doctor.Login);
  //pedidos panoramax
  router.get('/Doctor/:Id/GetOrdersPanoramax',
        controllers.Doctor.GetOrdersPanoramax);

  router.post('/Doctor/NotificacionUno',
    controllers.Doctor.NotificacionUno);

  router.post('/Doctor/NotificacionTodos',
    controllers.Doctor.NotificacionTodos);

  return router;
};
