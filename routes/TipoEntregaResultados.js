module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/TipoEntregaResultados',
    controllers.TipoEntregaResultados.GetAll);
  router.get('/TipoEntregaResultados/:Id',
    controllers.TipoEntregaResultados.GetById);
  router.post('/TipoEntregaResultados',
    controllers.TipoEntregaResultados.Insert);
  router.put('/TipoEntregaResultados/:Id',
    controllers.TipoEntregaResultados.Update);
  router.delete('/TipoEntregaResultados/:Id',
    controllers.TipoEntregaResultados.Delete);

  return router;
};
