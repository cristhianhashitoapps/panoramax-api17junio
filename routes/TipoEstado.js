module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/TipoEstado',
    controllers.TipoEstado.GetAll);
  router.get('/TipoEstado/:Id',
    controllers.TipoEstado.GetById);
  router.post('/TipoEstado',
    controllers.TipoEstado.Insert);
  router.put('/TipoEstado/:Id',
    controllers.TipoEstado.Update);
  router.delete('/TipoEstado/:Id',
    controllers.TipoEstado.Delete);

  return router;
};
